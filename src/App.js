import React from 'react';
import './App.css';
import Login from './components/login/Login';
import Register from './components/register/Register';
import TopBar from './components/topbar/TopBar';
import Navbar from './components/navbar/Navbar';
import Dashboard from './components/dashboard/Dashboard';
import Personas from './components/personas/Personas';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import AddPersona from './components/add-persona/AddPersona';
import AddPersonaInfo from './components/add-persona/add-persona-info/AddPersonaInfo';
import PersonsListing from './components/personas/PersonsListing/PersonsListing';
import Micuenta from './components/micuenta/Micuenta';


function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Router>
        <TopBar/>
        <Navbar/>
        <Switch>
          <Route path="/login" component={Login}/>
          <Route path="/register" component={Register}/>
          <Route path="/dashboard" component={Dashboard}/>
          <Route path="/personas" component={Personas}/>
          <Route path="/addpersona" component={AddPersona}/>
          <Route path="/addpersonainfo" component={AddPersonaInfo}/>
          <Route path="/personslisting" component={PersonsListing}/>
          <Route path="/micuenta" component={Micuenta}/>
        </Switch>
      </Router>
    </MuiThemeProvider>
  );
}

export default App;

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      containedPrimary: {
        color: 'white',
        boxShadow: 'none',
      },
      containedSecondary: {
        color: 'white',
        boxShadow: 'none',
      },
      root: {
        width: '150px',
        height: '50px',
        textTransform: 'none',
      },
    },
    MuiAppBar: {
      root: {
        height: '71px',
        width: 'calc(100% - 105px)',
        boxShadow: 'none',
        borderBottomWidth: '1px',
        borderBottom: 'solid',
        borderBottomColor: '#e3e3e3',
      },
      colorPrimary: {
        color: 'black',
        backgroundColor: 'white',
      }
    },
    MuiDrawer: {
      paper: {
        width: '105px',
        boxShadow: '0px 1px 8px #e3e3e3',
      },
    },
    MuiListItem: {
      root: {
        color: '#9498A9',
        '&$selected': {
          color: '#5A8DEE',
          backgroundColor: '#ECF2FF',
          borderRight: '5px solid #5A8DEE',
          '&:hover' : {
            backgroundColor: '#ECF2FF',
          },
        },
      },
      button: {
        '&:hover' : {
          backgroundColor: '#ECF2FF',
        }
      },
    },
    MuiOutlinedInput: {
      root: {
        width: "200px",
      }
    },
  },
  typography: {
    fontFamily: [
      'Source Sans Pro',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  palette: {
    primary: { 
      main: '#5A8DEE',
    },
    secondary: {
      main: '#FAC56F',
    },
  },
});
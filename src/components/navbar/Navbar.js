import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Drawer, List, ListItem, Grid } from '@material-ui/core';
import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import PersonAddOutlinedIcon from '@material-ui/icons/PersonAddOutlined';
import DateRangeIcon from '@material-ui/icons/DateRange';
import SmsOutlinedIcon from '@material-ui/icons/SmsOutlined';
import GroupWorkOutlinedIcon from '@material-ui/icons/GroupWorkOutlined';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';

//import PersonAddIcon from '@material-ui/icons/PersonAdd';
import './navbar.css';

function Navbar() {

  const [active, setActive] = useState("dashboard");

  return (
    <div className="root">
      <Grid container direction="column" className="grid-container-navbar" spacing={6}>
        <Drawer
          className="drawer"
          variant="permanent"
          anchor="left"
        >
          <Grid item className="grid-item-navbar-general">
            <div><h4>Orquesti</h4></div>
            <List>
                  <ListItem button component={Link} to="/dashboard" 
                            selected={active === "dashboard"} 
                            onClick={() => setActive("dashboard")}>
                    <DashboardOutlinedIcon fontSize="large" className="navbar-icon"/>
                  </ListItem>
                  <ListItem button component={Link} to="/register" 
                            selected={active === "team"} 
                            onClick={() => setActive("team")}>
                    <GroupWorkOutlinedIcon fontSize="large" className="navbar-icon"/>
                  </ListItem>
                  <ListItem button component={Link} to="/personas" 
                            selected={active === "employee"} 
                            onClick={() => setActive("employee")}>
                    <PersonAddOutlinedIcon fontSize="large" className="navbar-icon"/>
                  </ListItem>
                  <ListItem button component={Link} to="/register" 
                            selected={active === "calendar"} 
                            onClick={() => setActive("calendar")}>
                    <DateRangeIcon fontSize="large" className="navbar-icon"/>
                  </ListItem>
                  <ListItem button component={Link} to="/login" 
                            selected={active === "chat"} 
                            onClick={() => setActive("chat")}>
                    <SmsOutlinedIcon fontSize="large" className="navbar-icon"/>
                  </ListItem>
            </List>
          </Grid>
          <Grid item className="grid-item-navbar-settings">
            <List>
              <ListItem button component={Link} to="/micuenta" 
                        selected={active === "account"} 
                        onClick={() => setActive("account")}
                        className="settings-navbar-button">
                <AccountCircleOutlinedIcon fontSize="large" className="navbar-icon"/>
              </ListItem>
              <ListItem button component={Link} to="/login" 
                        selected={active === "settings"} 
                        onClick={() => setActive("settings")}
                        className="settings-navbar-button">
                <SettingsOutlinedIcon fontSize="large" className="navbar-icon"/>
              </ListItem>
            </List>
          </Grid>
        </Drawer>

      </Grid>

    </div>
  )
}

export default Navbar;

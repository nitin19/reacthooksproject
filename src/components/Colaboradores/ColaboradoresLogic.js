import { useState, useRef } from 'react';

const ColaboradoresLogic = (props) => {  
		const [inputvalue1, setInputValue1] = useState('');
		const [inputvalue2, setInputValue2] = useState('');
		const [inputvalue3, setInputValue3] = useState('');
		const [selectedDate, setSelectedDate] = useState(new Date());
		const [hideClickBottomForm, setHideClickBottomForm] = useState(false);

		function toggle() {
		    setHideClickBottomForm(!hideClickBottomForm);
		}

		function createData(image,name, calories, fat, carbs, protein) {
	  		return { image, name, calories, fat, carbs, protein };
		}

	  	const rows = [
			createData('https://material-ui.com/static/images/avatar/1.jpg', 'Frozen yoghurt', 159, 6.0, 24, 4.0),
		  	createData('https://material-ui.com/static/images/avatar/1.jpg', 'Ice cream sandwich', 237, 9.0, 37, 4.3),
		  	createData('https://material-ui.com/static/images/avatar/1.jpg', 'Eclair', 262, 16.0, 24, 6.0),
		];
	  	const handleDateChange = date => {
	    	setSelectedDate(date);
	  	};
	  	const handleChangeInput1 = (event) => {
	    	setInputValue1(event.target.value);
	  	}
	  	const handleChangeInput2 = (event) => {
	    	setInputValue2(event.target.value);
	  	}
	  	const handleChangeInput3 = (event) => {
	    	setInputValue3(event.target.value);
	  	}
		const leftbottomForm = useRef(null);
		const rightbottomForm = useRef(null);
		const [leftstartendtime, setLeftStartEndTime] = useState(props);
		const [righttotalhours, setRightTotalHours] = useState(props);
	  	/*const [leftstarttime, setLeftStartTime] = React.useState('');
	  	const [leftendtime, setLeftEndTime] = React.useState('');*/
	  	/*const [rightstarttime, setRightStartTime] = React.useState('');
	  	const [rightendtime, setRightEndTime] = React.useState('');
	  	const [rightvar1, setRightVar1] = React.useState('');
	  	const [rightvar2, setRightVar2] = React.useState('');*/
  
	  	const handleClickEventLeftBottomForm = () => {
	      	const form = leftbottomForm.current;
	      	const leftstarttimevalue = form.left_start_time.value;
	      	const leftendtimevalue = form.left_end_time.value;
	      	const leftstartendtimevalue = leftstarttimevalue+'am-'+leftendtimevalue+'pm';
	      	setLeftStartEndTime(leftstartendtimevalue);
	      	/*setLeftStartTime(form.left_start_time.value);
	      	setLeftEndTime(form.left_end_time.value);*/
	  	}
	  	const handleClickEventRightBottomForm = () => {
	      	const form = rightbottomForm.current;
	      	setRightTotalHours(form.right_total_hours.value+' horas');
	      	/*setRightStartTime(form.right_start_time.value);
	      	setRightEndTime(form.right_end_time.value);
	      	setRightVar1(form.right_var_1.value);
	      	setRightVar2(form.right_var_2.value);*/
	      
		}

	return {
	    inputvalue1,
	    inputvalue2,
	    inputvalue3,
	    leftstartendtime,
	    righttotalhours,
	    handleChangeInput1,
	    handleChangeInput2,
	    handleChangeInput3,
	    leftbottomForm,
	    rightbottomForm,
	    handleClickEventLeftBottomForm,
	    handleClickEventRightBottomForm,
	    selectedDate,
	    handleDateChange,
	    hideClickBottomForm,
	    toggle,
	    createData,
	    rows,
  	}
};

export default ColaboradoresLogic;
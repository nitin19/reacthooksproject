import React from 'react';
import './Colaboradores.css';
import ColaboradoresTimeingForm from './ColaboradoresTimeingForm';
import {  
  Grid,
  TableRow, 
  TableHead, 
  TableContainer, 
  TableCell, 
  TableBody, 
  Table, 
  Paper
} from '@material-ui/core';
import ColaboradoresLogic from './ColaboradoresLogic';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1),
  },
  table: {
    minWidth: 650,
  },
}));

function ColaboradoresTable() {
  const classes = useStyles();
  const {
    leftstartendtime,
    righttotalhours,
    hideClickBottomForm,
    toggle,
    rows,
  } = ColaboradoresLogic();
  console.log(leftstartendtime);
  return (
    <>
      <Grid item xs={12} className="co-table-section">
        <TableContainer component={Paper} className="co-table-main-section">
          <Table className={classes.table} aria-label="caption table">
            {/*<caption>A barbone structure table example with a caption</caption>*/}
            <TableHead>
              <TableRow>
                <TableCell>Miembros {leftstartendtime}</TableCell>
                <TableCell align="right">2 Lun</TableCell>
                <TableCell align="right">3 Ma</TableCell>
                <TableCell align="right">4 Mi</TableCell>
                <TableCell align="right">5 Ju</TableCell>
                <TableCell align="right">6 Vi</TableCell>
                <TableCell align="right">7 Sa</TableCell>
                <TableCell align="right">8 Do</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map(row => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    <img className="co-table-data-user-img" src={row.image} alt="" />{row.name}
                  </TableCell>
                  <TableCell align="right" onClick={toggle}><p className="co-start-end-time-text">{leftstartendtime}</p><p className="co-table-total-hours-text">{righttotalhours}</p></TableCell>
                  <TableCell align="right"></TableCell>
                  <TableCell align="right"></TableCell>
                  <TableCell align="right"></TableCell>
                  <TableCell align="right"></TableCell>
                  <TableCell align="right"></TableCell>
                  <TableCell align="right"></TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      {hideClickBottomForm ?
        <ColaboradoresTimeingForm data={leftstartendtime}/>
      : <div></div>
      }
    </>
  )
}
export default ColaboradoresTable;
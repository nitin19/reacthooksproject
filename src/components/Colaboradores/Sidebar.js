import React from 'react';
import './Colaboradores.css';
import { 
  Button, 
  IconButton, 
  InputBase, 
  ListItem, 
  List, 
  ListItemAvatar, 
  Avatar, 
  ListItemText,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

function Sidebar() {
  const [selectedIndex, setSelectedIndex] = React.useState(1);
  //const [numberOfitemsShown, setNumberOfitemsShown] = React.useState();
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };
  const items = [
    {
      id: '1',
      image: 'https://material-ui.com/static/images/avatar/1.jpg',
      name: 'Juan Hernández',
      status: 'Active',
    },
    {
      id: '2',
      image: 'https://material-ui.com/static/images/avatar/1.jpg',
      name: 'Juan Hernández',
      status: 'Active',
    },
  {
      id: '3',
      image: 'https://material-ui.com/static/images/avatar/1.jpg',
      name: 'Juan Hernández',
      status: 'Active',
  },
  {
      id: '4',
      image: 'https://material-ui.com/static/images/avatar/1.jpg',
      name: 'Juan Hernández',
      status: 'Active',
  },
  {
      id: '5',
      image: 'https://material-ui.com/static/images/avatar/1.jpg',
      name: 'Juan Hernández',
      status: 'Active',
  }
  ]
  return(
    <>
      <div className="component-logged-in co-side-content-section">
        <h2 className="co-side-top-head">Colaboradores</h2>
        <div className="search-section">
          <InputBase className="input" placeholder="Buscar..." inputProps={{ 'aria-label': 'Buscar' }}/>
          <IconButton type="submit" className="iconButton" aria-label="search"><SearchIcon /></IconButton>
        </div>
        <List component="nav" aria-label="main mailbox folders" className="co-user-list-root">
          {/*{items.slice(0, numberOfitemsShown).map(item =>(*/} 
          {items.map(item =>(
            <ListItem button selected={selectedIndex === 0} onClick={event => handleListItemClick(event, 0)} key={item.id}>
            <ListItemAvatar><Avatar alt="Remy Sharp" src={item.image} /></ListItemAvatar>
            <ListItemText primary={item.name} secondary={<React.Fragment>{item.status}</React.Fragment>}/></ListItem>
          ))}
        </List>
        <div className="agregar-btn-section">
          <Button variant="contained" color="primary" size="large" id="agregar-btn" className="button">+ Agregar persona</Button>
        </div>
      </div>
    </>
  )
}
export default Sidebar;
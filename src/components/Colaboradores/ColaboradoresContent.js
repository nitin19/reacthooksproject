import React from 'react';
import './Colaboradores.css';
import ColaboradoresTable from './ColaboradoresTable.js';
import {Grid} from '@material-ui/core';
import {MuiPickersUtilsProvider,KeyboardDatePicker} from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ColaboradoresLogic from './ColaboradoresLogic';

function ColaboradoresContent() {
  const {
    selectedDate,
    handleDateChange
  } = ColaboradoresLogic();
  return(
    <>
      <div className="component-logged-in co-right-content-section">
        <h2 className="co-right-content-top-head">Bienvenido a su calendario de Orquesti</h2>
        <p className="co-right-sub-text">Administre a sus equipos por unidades de trabajo y verifique los tiempos laborales</p>
        <div className="co-datepicker-section">
          <Grid item xs={4}></Grid>
          <Grid item xs={4}>
            <span className="co-datepicker-left-icon"><ArrowBackIosIcon/></span>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                margin="normal"
                id="date-picker-dialog"
                className="co-datepicker co-input-field"
                format="MM/dd/yyyy"
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
            <span className="co-datepicker-right-icon"><ArrowForwardIosIcon /></span>
          </Grid>
          <Grid item xs={4}></Grid>
        </div>
        <Grid item xs={12} className="co-table-header-section">
          <Grid item xs={6} className="co-left-table-header-section">
            <h2>Almacenes y Limpieza</h2>
          </Grid>
          <Grid item xs={6} className="co-right-table-header-section">
          <ExpandMoreIcon />
          </Grid>
        </Grid>
        <ColaboradoresTable />
      </div>
    </>
  )
}
export default ColaboradoresContent;
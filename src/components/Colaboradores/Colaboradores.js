import React from 'react';
import './Colaboradores.css';
import Sidebar from './Sidebar.js';
import ColaboradoresContent from './ColaboradoresContent.js';
import {Grid} from '@material-ui/core';
//import BlockEvents from './BlockEvent';
function Colaboradores() {
  return (
    <div>
      <Grid container className="container">
        <Grid item xs={3} className="co-sidebar-section">
          <Sidebar />
        </Grid>
        <Grid item xs={9}>
          <ColaboradoresContent />
        </Grid>
      </Grid>
    </div>
  )
}
export default Colaboradores;
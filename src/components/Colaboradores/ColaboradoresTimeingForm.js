import React from 'react';
import './Colaboradores.css';
import ColaboradoresLogic from './ColaboradoresLogic';
import { 
  Grid,
  InputLabel,
  TextField,
  Button
} from '@material-ui/core';

function ColaboradoresTimeingForm() {
  const {
    inputvalue1,
    inputvalue2,
    inputvalue3,
    leftstartendtime,
    righttotalhours,
    handleChangeInput1,
    handleChangeInput2,
    handleChangeInput3,
    leftbottomForm,
    rightbottomForm,
    handleClickEventLeftBottomForm,
    handleClickEventRightBottomForm
  } = ColaboradoresLogic();
  return (
    <>
      <div className="co-right-bottom-section">
        {/*<BlockEvents />*/}
        <h2 className="co-right-bottom-head">Detalle de horario</h2>
        <p className="co-right-bottom-text">Lunes  2 de diciembre</p>
        <Grid item xs={12} className="co-popup-head-text-section">
          <div className="co-sub-header-section">
            <Grid item xs={4} className="co-cuenta-confirm-popup-left-section">
              <img className="co-profile-img" src="https://material-ui.com/static/images/avatar/1.jpg" alt="" />
              <span className="co-profile-text">Manuel Campos</span>
            </Grid>
            <Grid item xs={8} className="co-cuenta-confirm-popup-right-section">
              <span className="co-horas-asignadas">Horas asignadas</span>
              <input type="text" name="itemquantity" id="itemquantity" className="co-cuenta-facturacion-itemquantity" onChange={handleChangeInput1} value={inputvalue1} placeholder="8 h"/>
              <span className="co-horas-asignadas">Horas realizadas</span>
              <input type="text" name="itemquantity" id="itemquantity_new1" className="co-cuenta-facturacion-itemquantity" onChange={handleChangeInput2} value={inputvalue2} placeholder="8h 4m"/>
              <span className="co-horas-asignadas">Costo total</span>
              <input type="text" name="itemquantity" id="itemquantity_new2" className="co-cuenta-facturacion-itemquantity" onChange={handleChangeInput3} value={inputvalue3} placeholder="$180"/>
            </Grid>
          </div>
        </Grid>
        <Grid item xs={12} className="co-timetable-add-form-section">
          <Grid item xs={5} className="co-timetable-left-form-section">
            <Grid item xs={4}>
              <h2 className="co-timetable-left-head-text">Horario establecido</h2>
            </Grid>
            <Grid item xs={8}>
              <form ref={leftbottomForm}>
                <div className="co-timetable-left-subform-section">
                  <Grid item xs={6}>
                    <InputLabel className="co-cuenta-input-field-label-section" id="co-cuenta-nombre-input-field-label-section">Entrada</InputLabel>
                    <div className="co-cuenta-input-field-section">
                        <TextField placeholder="8:00 am" name="left_start_time" className="co-input-field"/>
                    </div>
                  </Grid>
                  <Grid item xs={6}>
                    <InputLabel className="co-cuenta-input-field-label-section" id="co-cuenta-nombre-input-field-label-section">Salida</InputLabel>
                    <div className="co-cuenta-input-field-section">
                        <TextField placeholder="5:00 pm" name="left_end_time" className="co-input-field"/>
                    </div>
                  </Grid>
                </div>
              </form>
              <div className="co-agregar-left-bottom-btn-section">
                <Button variant="contained" color="primary" size="large" id="co-agregar-left-bottom-btn" className="button" onClick={handleClickEventLeftBottomForm}>Guardar</Button>
              </div>
            </Grid>
          </Grid>
          <Grid item xs={7} className="co-timetable-right-form-section">
            <Grid item xs={4}>
              <h2 className="co-timetable-right-head-text">Horario establecido</h2>
              <p className="co-variacion-text">Variación</p>
              <p className="co-total-hours-text">Total de horas</p>
            </Grid>
            <Grid item xs={4}>
              <form ref={rightbottomForm}>
                <div className="co-timetable-right-between-section">
                  <Grid item xs={6}>
                    <InputLabel className="co-cuenta-input-field-label-section" id="co-cuenta-nombre-input-field-label-section">Entrada</InputLabel>
                    <div className="co-cuenta-input-field-section">
                        <TextField placeholder="8:14 am" name="right_start_time" className="co-input-field"/>
                    </div>
                    <div className="co-cuenta-input-field-section">
                        <TextField placeholder="-14 m" name="right_var_1" className="co-input-field"/>
                    </div>
                  </Grid>
                  <Grid item xs={6}>
                    <InputLabel className="co-cuenta-input-field-label-section" id="co-cuenta-nombre-input-field-label-section">Salida</InputLabel>
                    <div className="co-cuenta-input-field-section">
                        <TextField placeholder="4:10 pm" name="right_end_time" className="co-input-field"/>
                    </div>
                    <div className="co-cuenta-input-field-section">
                        <TextField placeholder="-50 m" name="right_var_2" className="co-input-field"/>
                    </div>
                  </Grid>
                </div>
                <Grid item xs={12} className="co-timetable-right-bottom-field-section">
                  <div className="co-cuenta-input-field-section">
                    <TextField placeholder="8:04 horas" name="right_total_hours" className="co-input-field"/>
                  </div>
                </Grid>
              </form>
            </Grid>
            <Grid item xs={4} className="co-timetable-right-subform-section">
              <div className="co-agregar-top-btn-section">
                <Button variant="contained" color="primary" size="large" id="co-agregar-top-btn" className="button" onClick={handleClickEventRightBottomForm}>Aprobar</Button>
                <Button variant="contained" color="primary" size="large" id="co-agregar-bottom-btn" className="button">Aprobar</Button>
              </div>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </>
  )
}
export default ColaboradoresTimeingForm;
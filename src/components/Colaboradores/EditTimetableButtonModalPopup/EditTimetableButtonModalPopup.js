import React from 'react';
import '../EditTimetableButtonModalPopup/EditTimetableButtonModalPopup.css';
import {
	Grid, 
	TextField, 
	Button, 
	FormControlLabel, 
	FormLabel, 
	RadioGroup, 
	Radio, 
	FormControl,
	MenuItem,
	InputLabel,
	Select,
	Modal,
	Backdrop,
	Fade
} from '@material-ui/core';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import {MuiPickersUtilsProvider,KeyboardDatePicker,} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

function EditTimetableButtonModalPopup(){
	const [open, setOpen] = React.useState(false);
	const [num, setNum] = React.useState('');
	const [quantity, setQuantity] = React.useState('');
	const handleOpen = () => {
    	setOpen(true);
  	};
	const handleClose = () => {
	    setOpen(false);
	};
	const handleNumChange = (event) => {
        setNum(event.target.value);
    }
	const handleQuantityChange = (event) => {
        setQuantity(event.target.value);
    }
	const inputLabel = React.useRef(null);
  	const [labelWidth, setLabelWidth] = React.useState(0);
	return (
		<>
			<div className="agregar-btn-section">
			    <Button variant="contained" color="primary" size="large" id="agregar-btn" onClick={handleOpen} className="button">+ Agregar persona</Button>
			</div>
			<Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className="modal"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className="paper">
                        <Grid item xs={12} className="et-popup-header-section">
		                  <Grid item xs={11} className="et-popup-head-text-section">
		                    <h2 className="et-popup-head">Semana Dic 2 - Dic 8, 2019</h2>
		                    <p className="et-popup-header-sub-text">Configure el horario de su empleado para la semana activa</p>
		                    <div className="et-sub-header-section">
			                    <Grid item xs={4} className="mi-cuenta-confirm-popup-left-section">
	                                <img className="et-profile-img" src="https://material-ui.com/static/images/avatar/1.jpg" alt="" />
			            			<span className="et-profile-text">Manuel Campos</span>
	                            </Grid>
	                            <Grid item xs={8} className="mi-cuenta-confirm-popup-right-section">
	                                <span className="et-horas-asignadas">Horas asignadas de este periodo</span>
	                                <input type="number" name="itemquantity" id="itemquantity" className="mi-cuenta-facturacion-itemquantity" value={quantity} onChange={handleQuantityChange} min="1" max="10"/>
	                                <span className="et-horas-asignadas">Costo total</span>
	                                <input type="number" name="itemquantity" id="itemquantity_new" className="mi-cuenta-facturacion-itemquantity" value={quantity} onChange={handleQuantityChange} min="1" max="10"/>
	                            </Grid>
	                        </div>
		                  </Grid>
		                  <Grid item xs={1} className="et-popup-close-btn-section">
		                    <div className="et-save-popup-close-section">
		                      <Button variant="contained" color="primary" size="large" id="et-close-save-popup-btn" onClick={handleClose} className="button">x</Button>
		                    </div>
		                  </Grid>
		                </Grid>
                        <Grid item xs={12} className="mi-cuenta-confirm-popup-section">
                        	<Grid item xs={8} className="et-table-section">
	                            <Grid item xs={3} className="et-user-days-name">
	                            	<h2 className="et-days_name">Lunes</h2>
	                        		<h2 className="et-days_name">Martes</h2>
	                        		<h2 className="et-days_name">Miércoles</h2>
	                        		<h2 className="et-days_name">Jueves</h2>
	                        		<h2 className="et-days_name">Viernes</h2>
	                            </Grid>
	                            <Grid item xs={4} className="et-user-start-time">
	                            	<div className="input-field-section et-cuenta-input-field-section">
			                            <Select
		                                    id="demo-customized-select"
		                                    value={num}
		                                    onChange={handleNumChange}
		                                    labelId="mi-cuenta-espacios-input-field-label-section" 
		                                    labelWidth={labelWidth}
		                                    placeholder="ssdsdsd"
		                                	>
		                                    <MenuItem value=""><em>None</em></MenuItem>
		                                    <MenuItem value={10}>10</MenuItem>
		                                    <MenuItem value={20}>20</MenuItem>
		                                    <MenuItem value={30}>30</MenuItem>
		                                    <MenuItem value={40}>40</MenuItem>
		                                    <MenuItem value={50}>50</MenuItem>
		                                    <MenuItem value={60}>60</MenuItem>
		                                    <MenuItem value={70}>70</MenuItem>
		                                    <MenuItem value={80}>80</MenuItem>
		                                    <MenuItem value={90}>90</MenuItem>
		                                    <MenuItem value={100}>100</MenuItem>
		                                </Select>
		                            </div>
	                            </Grid>
	                            <Grid item xs={1} className="et-extra-text-section">
	                            	<p className="et-extra-text">a</p>
	                            </Grid>
	                            <Grid item xs={4} className="et-user-end-time">
	                            	<div className="input-field-section et-cuenta-input-field-section">
			                            <Select
		                                    id="demo-customized-select"
		                                    value={num}
		                                    onChange={handleNumChange}
		                                    labelId="mi-cuenta-espacios-input-field-label-section" 
		                                    labelWidth={labelWidth}
		                                    placeholder="ssdsdsd"
		                                	>
		                                    <MenuItem value=""><em>None</em></MenuItem>
		                                    <MenuItem value={10}>10</MenuItem>
		                                    <MenuItem value={20}>20</MenuItem>
		                                    <MenuItem value={30}>30</MenuItem>
		                                    <MenuItem value={40}>40</MenuItem>
		                                    <MenuItem value={50}>50</MenuItem>
		                                    <MenuItem value={60}>60</MenuItem>
		                                    <MenuItem value={70}>70</MenuItem>
		                                    <MenuItem value={80}>80</MenuItem>
		                                    <MenuItem value={90}>90</MenuItem>
		                                    <MenuItem value={100}>100</MenuItem>
		                                </Select>
		                            </div>
	                            </Grid>
	                        </Grid>
	                        <Grid item xs={4} className="et-form-section"></Grid>
                        </Grid>

                    </div>
                </Fade>
            </Modal>
		</>
	)
}

export default EditTimetableButtonModalPopup;
import React from 'react';
import '../Micuenta.css';
import userprofile from '../../../images/userimage/userprofile.png';
import { Button } from '@material-ui/core';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PhoneInTalkIcon from '@material-ui/icons/PhoneInTalk';

class MicuentaSidebar extends React.Component {
	state = {
		children: '',
		value: '', 
		index: '',
		other: ''
	}
	render() {
    	return (
    		<>	
    			<div className="mi-cuenta-sidebar-section">
    				<div className="mi-cuenta-user-img-section">
    					<img src={userprofile} alt="" />
    				</div>
    				<h2 className="mi-cuenta-user-profile-name">Laura Hernández</h2>
    				<p className="mi-cuenta-user-role">Gerente</p>
    				<p className="mi-cuenta-user-desc">Rol <span className="mi-cuenta-user-type-text"><a href='/'>Supervisor</a></span> en Alamcenes FC</p>
    				<div className="mi-cuenta-divider-top"></div>
    				<ul className="mi-cuenta-contact-info-section">
    					<li><span className="mi-cuenta-contact-icons"><MailOutlineIcon /></span>ejemplo@gmail.com</li>
    					<li><span className="mi-cuenta-contact-icons"><PhoneInTalkIcon /></span>8876-5432</li>
    				</ul>
    				<div className="mi-cuenta-divider-bottom"></div>
    				<p className="mi-cuenta-before-text">Licencia</p>
    				<div className="mi-cuenta-plan-basico-btn-sec">
    					<Button variant="contained" color="primary" size="large" id="mi-cuenta-plan-basico-btn" className="button">Plan básico</Button>
    				</div>
    			</div>
    		</>
    	)
    }
}
export default MicuentaSidebar;
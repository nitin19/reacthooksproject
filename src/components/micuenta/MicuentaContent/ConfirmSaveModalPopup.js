import React from 'react';
import {Grid, Button, Modal, Backdrop, Fade} from '@material-ui/core';
import savepopupimg from '../../../images/confetti.png';
function ConfirmSaveModalPopup() {
    const [opensave, setSaveOpen] = React.useState(false);
    const handleSaveOpen = () => {
        setSaveOpen(true);
    };
    const handleSaveClose = () => {
        setSaveOpen(false);
    };

    return(
        <>
            <Button variant="contained" color="primary" size="large" id="close-save-popup-btn" onClick={handleSaveOpen} className="mi-cuenta-confirm-popup-compra-button">Comprar</Button>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className="modal"
                open={opensave}
                onClose={handleSaveClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={opensave}>
                    <div className="paper">
                        <Grid item xs={12}>
                            <div className="save-popup-close-section">
                                <Button variant="contained" color="primary" size="large" id="close-save-popup-btn" onClick={handleSaveClose} className="button">x</Button>
                            </div>
                            <div className="popup-content-section">
                                <div className="image-save-section">
                                    <img src={savepopupimg} alt="celebration"/>
                                </div>
                                <p className="save-message">Su compra se ha completado correctamente y su plan ha sido actualizado</p>
                                <p className="mi-cuenta-confirm-popup-compra-sub-text">¡Agregue a sus nuevos empleados ahora!</p>
                                <Button variant="contained" color="primary" size="large" id="agregar-btn" className="button">Volver a Persona</Button>
                            </div>
                        </Grid>
                    </div>
                </Fade>
            </Modal>
        </>
    )
}
export default ConfirmSaveModalPopup;
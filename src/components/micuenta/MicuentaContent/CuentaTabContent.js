import React from 'react';
import '../Micuenta.css'; 
import {Grid, InputLabel, TextField, Button} from '@material-ui/core';

function CuentaTabContent () {
    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);
    return(
        <Grid item xs={12}>
            <Grid item xs={6}>
                <InputLabel ref={inputLabel} className="mi-cuenta-input-field-label-section" id="mi-cuenta-nombre-input-field-label-section">Nombre</InputLabel>
                <div className="input-field-section mi-cuenta-input-field-section">
                    <TextField placeholder="Laura Hernández" labelId="mi-cuenta-nombre-input-field-label-section" labelWidth={labelWidth}/>
                </div>
            </Grid>
            <Grid item xs={6}>
                <InputLabel ref={inputLabel} className="mi-cuenta-input-field-label-section" id="mi-cuenta-correo-input-field-label-section">Correo</InputLabel>
                <div className="input-field-section mi-cuenta-input-field-section">
                    <TextField placeholder="ejemplo@gmail.com" labelId="mi-cuenta-correo-input-field-label-section" labelWidth={labelWidth}/>
                </div>
            </Grid>
            <Grid item xs={12} className="mi-cuenta-password-field-btn-section">
                <Grid item xs={6} className="mi-cuenta-password-field-section">
                    <InputLabel ref={inputLabel} className="mi-cuenta-input-field-label-section" id="mi-cuenta-constrasena-input-field-label-section">Constraseña</InputLabel>
                    <div className="input-field-section mi-cuenta-input-field-section">
                        <TextField placeholder="********" labelId="mi-cuenta-constrasena-input-field-label-section" labelWidth={labelWidth}/>
                    </div>
                </Grid>
                <Grid item xs={6} className="mi-cuenta-password-btn-section">
                    <Button className="mi-cuenta-cambiar-btn">Cambiar</Button>
                </Grid>
            </Grid>
            <Grid item xs={12} className="mi-cuenta-submit-btn-section">
                <Grid item xs={6} className="mi-cuenta-left-btn-section">
                    <Button className="mi-cuenta-red-border-btn">Eliminar cuenta</Button>
                </Grid>
                <Grid item xs={6} className="mi-cuenta-right-btn-section"> 
                    <Button className="mi-cuenta-blue-color-btn">Guardar</Button>
                </Grid>
            </Grid>
        </Grid>
    )
}
export default CuentaTabContent;
import React from 'react';
import '../Micuenta.css'; 
import {Grid, CircularProgress, TextField, InputLabel} from '@material-ui/core';

function DatosDeUsoTabContent() {
    return(
        <Grid item xs={12} className="mi-cuenta-datosdeuso-section">
            <Grid item xs={1}></Grid>
            <Grid item xs={10}>
                <h2 className="mi-cuenta-datosdeuso-head-text">Licencias de usarios</h2>
                <p className="mi-cuenta-datosdeuso-subhead-text">El plan básico permite a los administradores  disponer de 30 usuarios para su uso.</p>
                <Grid item xs={12} className="mi-cuenta-datosdeuso-piechart-field-section">
                    <Grid item xs={6}>
                        <div className="mi-cuenta-datos-blue-piechart-section">
                            <p className="mi-cuenta-datos-percentage-text">25%</p>
                            <CircularProgress className="mi-cuenta-datos-first-blue-piechart" variant="static" value={25} size={130} color="primary"/>
                            <p className="mi-cuenta-datos-percentage-title-text">Espacios usados</p>
                        </div>
                        <div className="mi-cuenta-datos-lightgreen-piechart-section">
                            <p className="mi-cuenta-datos-percentage-text">25%</p>
                            <CircularProgress className="mi-cuenta-datos-second-lightgreen-piechart" variant="static" value={25} size={130} color="primary"/>
                            <p className="mi-cuenta-datos-percentage-title-text">Espacios disponibles</p>
                        </div>
                    </Grid>
                    <Grid item xs={6}>
                        <Grid item xs={12} className="mi-cuenta-datos-right-side-section">
                            <Grid item xs={6} className="mi-cuenta-datos-left-side-field-section">
                                <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-correo-input-field-label-section">Fecha de inicio</InputLabel>
                                <div className="input-field-section mi-cuenta-input-field-section" labelId="mi-cuenta-correo-input-field-label-section">
                                    <TextField placeholder="00 / mm / 2019"/>
                                </div>
                            </Grid>
                            <Grid item xs={6} className="mi-cuenta-datos-right-side-field-section">
                                <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-correo-input-field-label-section">Fecha de expiración</InputLabel>
                                <div className="input-field-section mi-cuenta-input-field-section" labelId="mi-cuenta-correo-input-field-label-section">
                                    <TextField placeholder="00/mm/2019" />
                                </div>
                            </Grid>
                        </Grid>
                        <div className="mi-cuenta-datos-question-link-section">
                            <a href="/" className="mi-cuenta-datos-question-link">¿Quieres adquirir más espacios a tu plan?</a>
                        </div>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={1}></Grid>
        </Grid>
    )
}
export default DatosDeUsoTabContent;
                                
                                
                                
                            
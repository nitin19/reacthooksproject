import React from 'react';
import {Grid, Button, Modal, Backdrop, Fade, InputLabel} from '@material-ui/core';
import '../Micuenta.css'; 
import ConfirmSaveModalPopup from './ConfirmSaveModalPopup'; 

function ConfirmModalPopup() {
    const [opensave, setSaveOpen] = React.useState(false);
    const [quantity, setQuantity] = React.useState('');
    const handleSaveOpen = () => {
        setSaveOpen(true);
    };
    const handleSaveClose = () => {
        setSaveOpen(false);
    };
    const handleQuantityChange = (event) => {
        setQuantity(event.target.value);
    }
    return(
        <>
            <Button className="mi-cuenta-facturacion-confirmar-compra-button" variant="contained" size="large" onClick={handleSaveOpen}>Confirmar compra</Button>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className="modal"
                open={opensave}
                onClose={handleSaveClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={opensave}>
                    <div className="paper">
                        <div className="save-popup-close-section">
                            <Button variant="contained" color="primary" size="large" id="close-save-popup-btn" onClick={handleSaveClose} className="button">x</Button>
                        </div>
                        <Grid item xs={12} className="mi-cuenta-confirm-popup-section">
                            <Grid item xs={7} className="mi-cuenta-confirm-popup-left-section">
                                <p className="mi-cuenta-facturacion-subhead-text">Datos de Facturación</p>
                                <p className="mi-cuenta-card-number-text">Mastercard ······· 30205</p>
                                <ul className="mi-cuenta-confirm-popup-card-details-section">
                                    <li className="mi-cuenta-facturacion-right-side-subtotal-text">Juan Hernández<p className="mi-cuenta-facturacion-right-side-amount-text">10/20</p></li>
                                </ul>
                                <p className="mi-cuenta-facturacion-subhead-text">Información de Pago</p>
                                <Grid item xs={12} className="mi-cuenta-confirm-popup-information-detail-section">
                                    <Grid item xs={6} className="mi-cuenta-confirm-popup-left-sec">
                                        <p className="mi-cuenta-card-number-text">Juan Hernández</p>
                                        <p className="mi-cuenta-confirm-popup-card-person-address">Costa Rica, Heredia, San Rafael , Calle 4</p>
                                    </Grid>
                                    <Grid item xs={6} className="mi-cuenta-confirm-popup-right-sec">
                                        <p className="mi-cuenta-confirm-popup-left-sec-right-id-head">ID</p>
                                        <p className="mi-cuenta-confirm-popup-card-person-address">00000000</p>
                                        <p className="mi-cuenta-confirm-popup-left-sec-right-id-head">Código postal</p>
                                        <p className="mi-cuenta-confirm-popup-card-person-address">00000000</p>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={5} className="mi-cuenta-confirm-popup-right-section">
                                <div className="mi-cuenta-facturacion-right-side-main-section">
                                    <h2 className="mi-cuenta-facturacion-right-side-head-text">Orden de Compra</h2>
                                    <div className="mi-cuenta-facturacion-close-button-section">
                                        <Button className="mi-cuenta-facturacion-close-button">X</Button>
                                    </div>
                                    <Grid item xs={12} className="mi-cuenta-facturacion-itemdetails-section">
                                        <Grid item xs={6} className="mi-cuenta-facturacion-item-field-input">
                                            <p className="mi-cuenta-facturacion-right-side-item-text">Añadir nuevos usuarios</p>
                                        </Grid>
                                        <Grid item xs={6} className="mi-cuenta-facturacion-quantity-field-input">
                                        <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-facturacion-quantity-input-field-label-section">Cantidad</InputLabel>
                                            <input type="number" name="itemquantity" id="itemquantity" className="mi-cuenta-facturacion-itemquantity" value={quantity} onChange={handleQuantityChange} min="1" max="10"/>
                                        </Grid>
                                    </Grid>
                                    <ul className="mi-cuenta-facturacion-subtotal-section">
                                        <li className="mi-cuenta-facturacion-right-side-subtotal-text">Subtotal<p className="mi-cuenta-facturacion-right-side-amount-text">$00</p></li>
                                        <li className="mi-cuenta-facturacion-right-side-subtotal-text">Total<p className="mi-cuenta-facturacion-right-side-amount-text">$00</p></li>
                                    </ul> 
                                </div>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} className="mi-cuenta-confirm-popup-bottom-btn-section">
                            <Grid item xs={6}>
                                <p className="mi-cuenta-confirm-popup-iratras-btn-text">Ir atrás</p>
                            </Grid>
                            <Grid item xs={6}>
                                <ConfirmSaveModalPopup />
                            </Grid>
                        </Grid>
                    </div>
                </Fade>
            </Modal>
        </>
    )
}
export default ConfirmModalPopup;
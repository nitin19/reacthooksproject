import React from 'react';
import '../Micuenta.css'; 
import {Grid, InputLabel, TextField, Button, Select, MenuItem} from '@material-ui/core';

function SuscripcionTabContent() {
    const [num, setNum] = React.useState('');
    const [mensual, setMensual] = React.useState('');
    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    const handleMensualChange = (event) => {
        setMensual(event.target.value);
    }
    const handleNumChange = (event) => {
        setNum(event.target.value);
    }
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    return(
        <Grid item xs={12} className="mi-cuenta-suscripcion-section">
            <h2 className="mi-cuenta-suscripcion-head-text">Suscripción: Plan básico</h2>
            <p className="mi-cuenta-suscripcion-subhead-text">Se aplicará automáticamente para períodos futuros a partir del 1/10/2019</p>
            <Grid item xs={12} className="mi-cuenta-suscripcion-main-form-section">
                <Grid item xs={8}>
                    <Grid item xs={12} className="mi-cuenta-subscription-form-field-sec">
                        <Grid item xs={4} className="mi-cuenta-suscripcion-espacios-field-sec">
                            <InputLabel ref={inputLabel} className="mi-cuenta-input-field-label-section" id="mi-cuenta-espacios-input-field-label-section">Espacios</InputLabel>
                            <div className="input-field-section mi-cuenta-input-field-section">
                                <Select
                                    id="demo-customized-select"
                                    value={num}
                                    onChange={handleNumChange}
                                    labelId="mi-cuenta-espacios-input-field-label-section" 
                                    labelWidth={labelWidth}
                                >
                                    <MenuItem value=""><em>None</em></MenuItem>
                                    <MenuItem value={10}>10</MenuItem>
                                    <MenuItem value={20}>20</MenuItem>
                                    <MenuItem value={30}>30</MenuItem>
                                    <MenuItem value={40}>40</MenuItem>
                                    <MenuItem value={50}>50</MenuItem>
                                    <MenuItem value={60}>60</MenuItem>
                                    <MenuItem value={70}>70</MenuItem>
                                    <MenuItem value={80}>80</MenuItem>
                                    <MenuItem value={90}>90</MenuItem>
                                    <MenuItem value={100}>100</MenuItem>
                                </Select>
                            </div>
                        </Grid>
                        <Grid item xs={4} className="mi-cuenta-suscripcion-periocidad-field-sec">
                            <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-periocidad-input-field-label-section">Periocidad</InputLabel>
                            <div className="input-field-section mi-cuenta-input-field-section">
                                <Select
                                    id="demo-customized-select"
                                    value={mensual}
                                    onChange={handleMensualChange}
                                    labelId="mi-cuenta-periocidad-input-field-label-section" 
                                    labelWidth={labelWidth}
                                >
                                    <MenuItem value=""><em>None</em></MenuItem>
                                    <MenuItem value="mensual1">Mensual1</MenuItem>
                                    <MenuItem value="mensual2">Mensual2</MenuItem>
                                    <MenuItem value="mensual3">Mensual3</MenuItem>
                                    <MenuItem value="mensual4">Mensual4</MenuItem>
                                    <MenuItem value="mensual5">Mensual5</MenuItem>
                                </Select>
                            </div>
                        </Grid>
                        <Grid item xs={4} className="mi-cuenta-suscripcion-montoestimado-field-sec">
                            <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-montoestimado-input-field-label-section">Monto estimado</InputLabel>
                            <div className="input-field-section mi-cuenta-input-field-section">
                                <TextField placeholder="$ 00" labelId="mi-cuenta-montoestimado-input-field-label-section" labelWidth={labelWidth}/>
                            </div>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <div className="input-field-section mi-cuenta-suscripcion-main-input-field-section">
                            <InputLabel className="mi-cuenta-suscripcion-id-input-field-label-section" id="mi-cuenta-suscripcionid-input-field-label-section">Suscripción ID</InputLabel>
                            <div className="mi-cuenta-input-field-section">
                                <TextField placeholder="XXX - XXXXXX" className="mi-cuenta-suscripcion-id-input" labelId="mi-cuenta-suscripcionid-input-field-label-section" labelWidth={labelWidth}/>
                            </div>
                            <Grid item xs={12} className="mi-cuenta-suscripcion-last-inputs-field-sec">
                                <Grid item xs={4}>
                                    <InputLabel className="mi-cuenta-suscripcion-input-field-label-section" id="mi-cuenta-estado-input-field-label-section">Estado</InputLabel>
                                    <div className="input-field-section mi-cuenta-suscripcion-input-field-section">
                                        <TextField placeholder="Activa" className="mi-cuenta-suscripcion-input" labelId="mi-cuenta-estado-input-field-label-section" labelWidth={labelWidth}/>
                                    </div>
                                </Grid>
                                <Grid item xs={4}>
                                    <InputLabel className="mi-cuenta-suscripcion-input-field-label-section" id="mi-cuenta-usuarios-input-field-label-section">Usuarios</InputLabel>
                                    <div className="input-field-section mi-cuenta-suscripcion-input-field-section">
                                        <TextField placeholder="30" className="mi-cuenta-suscripcion-input" labelId="mi-cuenta-usuarios-input-field-label-section" labelWidth={labelWidth}/>
                                    </div>
                                </Grid>
                                <Grid item xs={4}>
                                    <InputLabel className="mi-cuenta-suscripcion-input-field-label-section" id="mi-cuenta-costo-input-field-label-section">Costo</InputLabel>
                                    <div className="input-field-section mi-cuenta-suscripcion-input-field-section">
                                        <TextField placeholder="$ 00" className="mi-cuenta-suscripcion-input" labelId="mi-cuenta-costo-input-field-label-section" labelWidth={labelWidth}/>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    </Grid>
                    <div className="mi-cuenta-suscripcion-cancelar-btn-section">
                        <Button className="mi-cuenta-suscripcion-cancelar-btn">Cancelar plan</Button>
                    </div>
                </Grid>
                <Grid item xs={4} className="mi-cuenta-suscripcion-aumentar-btn-section">
                    <Button className="mi-cuenta-suscripcion-aumentar-btn">Aumentar plan</Button>
                </Grid>
            </Grid>
        </Grid>
    )
}
export default SuscripcionTabContent;
                                
                                
                                
                            
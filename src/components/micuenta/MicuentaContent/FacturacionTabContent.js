import React from 'react';
import '../Micuenta.css'; 
import {Grid, InputLabel, TextField, Button, Select, MenuItem} from '@material-ui/core';
import ConfirmModalPopup from './ConfirmModalPopup'; 

function FacturacionTabContent() {
    const [quantity, setQuantity] = React.useState('');
    const [costarica, setCostarica] = React.useState('');
    const [heredia, setHeredia] = React.useState('');
    const [sanrafael, setSanrafael] = React.useState('');
    const handleQuantityChange = (event) => {
        setQuantity(event.target.value);
    }
    const handleCostaricaChange = (event) => {
        setCostarica(event.target.value);
    }
    const handleHerediaChange = (event) => {
        setHeredia(event.target.value);
    }
    const handleSanRafaelChange = (event) => {
        setSanrafael(event.target.value);
    }
    
    return(
    	<Grid item xs={12} className="mi-cuenta-facturacion-section">
    		<Grid item xs={8}>
    			<p className="mi-cuenta-facturacion-subhead-text">Datos de Facturación</p>
    			<Grid item xs={12} className="mi-cuenta-facturacio-form-first-section">
        			<Grid item xs={6} className="mi-cuenta-facturacio-form-left-section">
        				<InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-nombrecompleto-input-field-label-section">Nombre completo</InputLabel>
                        <div className="input-field-section mi-cuenta-input-field-section">
                            <TextField placeholder="Laura Hernández" labelId="mi-cuenta-nombrecompleto-input-field-label-section"/>
                        </div>
                        <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-correo-input-field-label-section">Correo</InputLabel>
                        <div className="input-field-section mi-cuenta-input-field-section" labelId="mi-cuenta-correo-input-field-label-section">
                            <TextField placeholder="ejemplo@gmail.com"/>
                        </div>
                        <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-estado-input-field-label-section">Estado / Provincia</InputLabel>
                        <div className="input-field-section mi-cuenta-input-field-section">
                            <Select
                                id="demo-customized-select"
                                value={heredia}
                                onChange={handleHerediaChange}
                                labelId="mi-cuenta-estado-input-field-label-section"
                            >
                                <MenuItem value=""><em>None</em></MenuItem>
                                <MenuItem value="heredia1">Heredia1</MenuItem>
                                <MenuItem value="heredia2">Heredia2</MenuItem>
                                <MenuItem value="heredia3">Heredia3</MenuItem>
                                <MenuItem value="heredia4">Heredia4</MenuItem>
                                <MenuItem value="heredia5">Heredia5</MenuItem>
                            </Select>
                        </div>
                        <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-direccion-input-field-label-section">Dirección</InputLabel>
                        <div className="input-field-section mi-cuenta-input-field-section">
                            <TextField placeholder="" labelId="mi-cuenta-direccion-input-field-label-section"/>
                        </div>
        			</Grid>
        			<Grid item xs={6} className="mi-cuenta-facturacio-form-right-section">
        				<InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-id-input-field-label-section">ID</InputLabel>
                        <div className="input-field-section mi-cuenta-input-field-section">
                            <TextField placeholder="0 0000 0000" labelId="mi-cuenta-id-input-field-label-section"/>
                        </div>
                        <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-pais-input-field-label-section">País</InputLabel>
                        <div className="input-field-section mi-cuenta-input-field-section">
                            <Select
                                id="demo-customized-select"
                                value={costarica}
                                onChange={handleCostaricaChange}
                                labelId="mi-cuenta-pais-input-field-label-section"
                            >
                                <MenuItem value=""><em>None</em></MenuItem>
                                <MenuItem value="costarica1">Costarica1</MenuItem>
                                <MenuItem value="costarica2">Costarica2</MenuItem>
                                <MenuItem value="costarica3">Costarica3</MenuItem>
                                <MenuItem value="costarica4">Costarica4</MenuItem>
                                <MenuItem value="costarica5">Costarica5</MenuItem>
                            </Select>
                        </div>
                        <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-ciudad-input-field-label-section">Ciudad</InputLabel>
                        <div className="input-field-section mi-cuenta-input-field-section">
                            <Select
                                id="demo-customized-select"
                                value={sanrafael}
                                onChange={handleSanRafaelChange}
                                labelId="mi-cuenta-ciudad-input-field-label-section"
                            >
                                <MenuItem value=""><em>None</em></MenuItem>
                                <MenuItem value="sanrafael1">SanRafael1</MenuItem>
                                <MenuItem value="sanrafael2">SanRafael2</MenuItem>
                                <MenuItem value="sanrafael3">SanRafael3</MenuItem>
                                <MenuItem value="sanrafael4">SanRafael4</MenuItem>
                                <MenuItem value="sanrafael5">SanRafael5</MenuItem>
                            </Select>
                        </div>
                        <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-codigopostal-input-field-label-section">Código Postal</InputLabel>
                        <div className="input-field-section mi-cuenta-input-field-section">
                            <TextField placeholder="" labelId="mi-cuenta-codigopostal-input-field-label-section"/>
                        </div>
        			</Grid>
        		</Grid>
                <p className="mi-cuenta-facturacion-subhead-text">Información de Pago</p>
                <Grid item xs={12} className="mi-cuenta-facturacio-form-second-section">
                    <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-numerodetarjeta-input-field-label-section">Número de tarjeta</InputLabel>
                    <div className="input-field-section mi-cuenta-input-field-section">
                        <TextField placeholder="000000000000000" labelId="mi-cuenta-numerodetarjeta-input-field-label-section"/>
                    </div>
                    <Grid item xs={12} className="mi-cuenta-facturacion-card-field-input-main-section">
                        <Grid item xs={6} className="mi-cuenta-facturacio-form-left-section">
                            <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-nombreentarjeta-input-field-label-section">Nombre en tarjeta</InputLabel>
                            <div className="input-field-section mi-cuenta-input-field-section">
                                <TextField placeholder="Laura Hernández" labelId="mi-cuenta-nombreentarjeta-input-field-label-section"/>
                            </div>
                        </Grid>
                        <Grid item xs={6} className="mi-cuenta-facturacio-form-right-section">
                            <Grid item xs={12} className="mi-cuenta-facturacion-card-field-input-sub-section">
                                <Grid item xs={6} className="mi-cuenta-facturacion-card-left-field-input-section">
                                    <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-expira-input-field-label-section">Expira</InputLabel>
                                    <div className="input-field-section mi-cuenta-input-field-section">
                                        <TextField placeholder="MM/YY" labelId='mi-cuenta-expira-input-field-label-section'/>
                                    </div>
                                </Grid>
                                <Grid item xs={6} className="mi-cuenta-facturacion-card-right-field-input-section">
                                    <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-cvv-input-field-label-section">CVV</InputLabel>
                                    <div className="input-field-section mi-cuenta-input-field-section">
                                        <TextField placeholder="000" labelId="mi-cuenta-cvv-input-field-label-section"/>
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
    		</Grid>
    		<Grid item xs={4} className="mi-cuenta-facturacion-right-side-section">
                <div className="mi-cuenta-facturacion-right-side-main-section">
                    <h2 className="mi-cuenta-facturacion-right-side-head-text">Orden de Compra</h2>
                    <div className="mi-cuenta-facturacion-close-button-section">
                        <Button className="mi-cuenta-facturacion-close-button">X</Button>
                    </div>
                    <Grid item xs={12} className="mi-cuenta-facturacion-itemdetails-section">
                        <Grid item xs={6} className="mi-cuenta-facturacion-item-field-input">
                            <p className="mi-cuenta-facturacion-right-side-item-text">Añadir nuevos usuarios</p>
                        </Grid>
                        <Grid item xs={6} className="mi-cuenta-facturacion-quantity-field-input">
                        <InputLabel className="mi-cuenta-input-field-label-section" id="mi-cuenta-facturacion-quantity-input-field-label-section">Cantidad</InputLabel>
                            <input type="number" name="itemquantity" id="itemquantity" className="mi-cuenta-facturacion-itemquantity" value={quantity} onChange={handleQuantityChange} min="1" max="10"/>
                        </Grid>
                    </Grid>
                    <ul className="mi-cuenta-facturacion-subtotal-section">
                        <li className="mi-cuenta-facturacion-right-side-subtotal-text">Subtotal<p className="mi-cuenta-facturacion-right-side-amount-text">$00</p></li>
                        <li className="mi-cuenta-facturacion-right-side-subtotal-text">Total<p className="mi-cuenta-facturacion-right-side-amount-text">$00</p></li>
                    </ul> 
                </div>
                <div className="mi-cuenta-facturacion-confirmar-compra-button-section">
                    <ConfirmModalPopup />
                </div>
    		</Grid>
    	</Grid>
    )
}
export default FacturacionTabContent;
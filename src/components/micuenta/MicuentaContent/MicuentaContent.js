import React from 'react';
import '../Micuenta.css'; 
import CuentaTabContent from './CuentaTabContent';
import DatosDeUsoTabContent from './DatosDeUsoTabContent';
import SuscripcionTabContent from './SuscripcionTabContent';
import FacturacionTabContent from './FacturacionTabContent';
import RecibosTabContent from './RecibosTabContent';
import {Tabs, Tab, Typography, AppBar} from '@material-ui/core';
import PropTypes from 'prop-types';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

function MicuentaContent() {
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    }
	return (
		<>	
			<div className="mi-cuenta-content-section">
			    <AppBar position="static" color="default" className="mi-cuenta-tab-header-section">
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        scrollable
                        scrollButtons="auto"
                    >
                        <Tab label="Cuenta" />
                        <Tab label="Datos de Uso" />
                        <Tab label="Suscripción" />
                        <Tab label="Facturación" />
                        <Tab label="Recibos" />
                    </Tabs>
                </AppBar>
                {
                    value === 0 && 
                    <TabContainer>
                        <CuentaTabContent />
                    </TabContainer>
                }
                {
                    value === 1 && 
                    <TabContainer>
                        <DatosDeUsoTabContent />
                    </TabContainer>
                }
                {
                    value === 2 && 
                    <TabContainer>
                        <SuscripcionTabContent />
                    </TabContainer>
                }
                {
                    value === 3 && 
                    <TabContainer>
                        <FacturacionTabContent />
                    </TabContainer>
                }
                {
                    value === 4 && 
                    <TabContainer>
                        <RecibosTabContent />
                    </TabContainer>
                }
			</div>
		</>
	)
}

export default MicuentaContent;
import React from 'react';
import '../Micuenta.css'; 
import {
	Grid,  
	Paper,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TablePagination,
	TableRow
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import PrintIcon from '@material-ui/icons/Print';

const columns = [
  { id: 'id', label: 'ID', minWidth: 80, align: 'center' },
  { id: 'categoria', label: 'Categoría', minWidth: 180, align: 'center' },
  {
    id: 'periodo',
    label: 'Período',
    minWidth: 135,
    align: 'center',
    format: value => value.toLocaleString(),
  },
  {
    id: 'monto',
    label: 'Monto',
    minWidth: 100,
    align: 'center',
    format: value => value.toLocaleString(),
  },
];
function createData(id, categoria, periodo, monto) {
  //const action = '<VisibilityIcon><EditIcon><PrintIcon>';
  return { id, categoria, periodo, monto};
}
const rows = [
  createData('00000', 'Facturación regular', '1 /12/2019 a 1/12/2018', '$30'),
  createData('00001', 'Facturación regular', '1 /12/2019 a 1/12/2018', '$30'),
];
const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  tableWrapper: {
    //maxHeight: 440,
    overflow: 'auto',
  },
});
function RecibosTabContent() {
	const classes = useStyles();
  	const [page, setPage] = React.useState(0);
  	const [rowsPerPage, setRowsPerPage] = React.useState(10);
  	const handleChangePage = (event, newPage) => {
    	setPage(newPage);
  	};

  	const handleChangeRowsPerPage = event => {
    	setRowsPerPage(+event.target.value);
    	setPage(0);
  	};
	return(
		<Grid item xs={12} className="mi-cuenta-recibos-section">
			<p className="mi-cuenta-facturacion-subhead-text">Todos los Recibos</p>
			<Paper className={classes.root}>
      			<div className={classes.tableWrapper}>
        			<Table stickyHeader aria-label="sticky table">
          				<TableHead>
				            <TableRow>
				              {columns.map(column => (
				                <TableCell
				                  key={column.id}
				                  align={column.align}
				                  style={{ minWidth: column.minWidth }}
				                >
				                  {column.label}
				                </TableCell>
				              ))}
				              <th class="MuiTableCell-root MuiTableCell-head MuiTableCell-alignCenter MuiTableCell-stickyHeader" scope="col"></th>
				            </TableRow>
          				</TableHead>
          				<TableBody>
            				{rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
              					return (
					                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
					                  {columns.map(column => {
					                    const value = row[column.id];
					                    return (
					                      <TableCell key={column.id} align={column.align}>
					                        {column.format && typeof value === 'number' ? column.format(value) : value}
					                      </TableCell>
					                    );
					                  })}
					                  <td class="MuiTableCell-root MuiTableCell-body MuiTableCell-alignCenter"><VisibilityIcon /><EditIcon /><PrintIcon /></td>
					                </TableRow>
              					);
            				})}
          				</TableBody>
        			</Table>
      			</div>
		      	<TablePagination
		        	rowsPerPageOptions={[10, 25, 100]}
		        	component="div"
		        	count={rows.length}
		        	rowsPerPage={rowsPerPage}
		        	page={page}
		        	onChangePage={handleChangePage}
		        	onChangeRowsPerPage={handleChangeRowsPerPage}
		      	/>
    		</Paper>
		</Grid>
	)
}
export default RecibosTabContent;
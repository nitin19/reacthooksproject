import React from 'react';
import './Micuenta.css';
import {Grid} from '@material-ui/core';
import MicuentaSidebar from './MicuentaSidebar/MicuentaSidebar';
import MicuentaContent from './MicuentaContent/MicuentaContent';

function Micuenta() {
	return (
    	<div>
      		<Grid container className="container">
            <Grid item sm={12}>
              <div className="component-logged-in mi-cuenta-main-heading-section">
                  <h2 className="mi-cuenta-main-head">Mi cuenta</h2>
              </div>
            </Grid>
        		<Grid item sm={4}>
          			<div className="component-logged-in mi-cuenta-main-sidebar-section">
            			<MicuentaSidebar />
          			</div>
        		</Grid>
        		<Grid item sm={8}>
                <div className="component-logged-in mi-cuenta-main-content-section">
                  <MicuentaContent />
                </div>
        		</Grid>
      		</Grid>
    	</div>
  )
}
export default Micuenta;
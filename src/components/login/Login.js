import React from 'react';
import './login.css';
import {Grid, TextField, FormControlLabel, Checkbox, Button} from '@material-ui/core';
import {Link} from 'react-router-dom';

function Login() {
  return (
    <div>
      <Grid container className="container">
        <Grid item xs={6}>
          <Grid container>
            <Grid item xs={4}></Grid>  
            <Grid item xs={4}>
              <div className="login-container">
                <div className="titulo">
                  <h1>¡Hola de nuevo!</h1>
                </div>
                <div className="sin-cuenta">
                <Link to="/register" className="link-register-sin-cuenta"><p>¿No tienes cuenta?</p></Link>
                </div> 
                <div className="form-container">
                  <div className="input-container">
                    <TextField
                      id="standard-required"
                      label="Correo"
                      defaultValue=""
                      className="TextField"
                      margin="normal"
                    />
                  </div>
                  <div className="input-container">
                    <TextField
                      id="standard-required"
                      label="Contraseña"
                      defaultValue=""
                      className="TextField"
                      type="password"
                      margin="normal"
                    />
                  </div>
                  <div className="container-debajo-inputs">
                    <Grid container
                          alignItems="center"
                          justify="space-between"
                    >
                      <Grid item>
                        <div className="texto-debajo-inputs-der">
                          <FormControlLabel
                            control={
                              <Checkbox value="recordarme" color="primary"/>
                            }
                            label="Recordarme"
                            classes={{label: 'texto-debajo-inputs'}}
                          />
                        </div>
                      </Grid>
                      <Grid item>
                        <div className="texto-debajo-inputs-der">
                          <Link to="/login" className="texto-debajo-inputs">¿Olvidó su contraseña?</Link>
                        </div>
                      </Grid>
                    </Grid>
                  </div>
                  <div className="container-botones">
                    <Button variant="contained" color="primary">Iniciar Sesión</Button>
                    <Link to="/register" className="login-register-btn"><Button color="inherit">Crear Cuenta</Button></Link>
                  </div>
                </div>
              </div>
            </Grid>  
            <Grid item xs={4}></Grid>  
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <div className="panel-derecha">
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default Login;
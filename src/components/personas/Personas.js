import React from 'react';
import './Personas.css';
import {Grid} from '@material-ui/core';
import PersonsListing from './PersonsListing/PersonsListing';
import PersonsDetails from './PersonsDetails/PersonsDetails';
import PersonsChat from './PersonsChat/PersonsChat';
//import {makeStyles } from '@material-ui/core/styles';

/*const useStyles = makeStyles(theme => ({
  inline: {
    display: 'inline',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '100%',
  },
  menu: {
    width: 200,
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
    marginTop:'30'
  },
  table: {
    minWidth: '100%',
    border: '1px solid #ccc',
  },
}));*/

function Personas() {
  return (
    <div>
      <Grid container className="container">
        <Grid item xs={3}>
          <div className="component-logged-in">
            <PersonsListing />
          </div>
        </Grid>
        
        <Grid item xs={6}>
          <Grid container className="container">
            <PersonsDetails />
         </Grid>
        </Grid>
        <Grid item xs={3}>
          <div className="component-logged-in chat-section">
            <PersonsChat />
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default Personas;
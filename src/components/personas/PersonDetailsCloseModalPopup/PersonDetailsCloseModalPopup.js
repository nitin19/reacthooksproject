import React from 'react';
import '../Personas.css';
import {Grid, Button, Modal, Backdrop, Fade} from '@material-ui/core';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

function PersonDetailsCloseModalPopup() {
    const [openclosepopup, setClosePopupOpen] = React.useState(false);
    const handleClosePopupOpen = () => {
        setClosePopupOpen(true);
    };
    const handleClosePopupClose = () => {
        setClosePopupOpen(false);
    };
    return(
        <>
            <HighlightOffIcon onClick={handleClosePopupOpen}/>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className="modal"
                open={openclosepopup}
                onClose={handleClosePopupClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={openclosepopup}>
                    <div className="paper">
                        <Grid item xs={12}>
                            <div className="save-popup-close-section">
                                <Button variant="contained" color="primary" size="large" id="close-save-popup-btn" onClick={handleClosePopupClose} className="button">x</Button>
                            </div>
                            <div className="close-popup-content-section">
                                <h2 className="close-popup-question-head">¿Estás seguro de eliminar la persona?</h2>
                                <p className="close-sub-message">Se borrará el usuario y toda la información guardada</p>
                                <Grid item xs={12} className="close-popup-button-section">
                                    <Grid item xs={6} className="first-button-section">
                                        <Button variant="contained" color="primary" size="large" id="close-popup-left-btn" className="button">Cancelar</Button>
                                    </Grid>
                                    <Grid item xs={6} className="second-button-section">
                                      <Button variant="contained" color="primary" size="large" id="close-popup-right-btn" className="button">Sí, eliminar</Button>
                                    </Grid>
                                </Grid>
                            </div>
                        </Grid>
                    </div>
                </Fade>
            </Modal>
        </>
    )
}
export default PersonDetailsCloseModalPopup;
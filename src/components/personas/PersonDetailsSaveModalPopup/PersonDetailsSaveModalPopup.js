import React from 'react';
import '../Personas.css';
import {Grid, Button, Modal, Backdrop, Fade} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import savepopupimg from '../../../images/confetti.png';

function PersonDetailsSaveModalPopup() {
    const [opensave, setSaveOpen] = React.useState(false);
    const handleSaveOpen = () => {
        setSaveOpen(true);
    };
    const handleSaveClose = () => {
        setSaveOpen(false);
    };
    return(
        <>
            <SaveIcon onClick={handleSaveOpen}/>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className="modal"
                open={opensave}
                onClose={handleSaveClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={opensave}>
                    <div className="paper">
                        <Grid item xs={12}>
                            <div className="save-popup-close-section">
                                <Button variant="contained" color="primary" size="large" id="close-save-popup-btn" onClick={handleSaveClose} className="button">x</Button>
                            </div>
                            <div className="popup-content-section">
                                <div className="image-save-section">
                                    <img src={savepopupimg} alt="celebration"/>
                                </div>
                                <p className="save-message">Su datos se han actualizado y guardado correctamente</p>
                                <Button variant="contained" color="primary" size="large" id="agregar-btn" className="button">Volver a Persona</Button>
                            </div>
                        </Grid>
                    </div>
                </Fade>
            </Modal>
        </>
    )
}
export default PersonDetailsSaveModalPopup;
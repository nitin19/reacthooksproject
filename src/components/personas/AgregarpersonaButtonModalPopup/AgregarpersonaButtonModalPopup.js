import React from 'react';
import '../Personas.css';
import {
	Grid, 
	TextField, 
	Button, 
	FormControlLabel, 
	FormLabel, 
	RadioGroup, 
	Radio, 
	FormControl,
	MenuItem,
	InputLabel,
	Select,
	Modal,
	Backdrop,
	Fade
} from '@material-ui/core';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import {MuiPickersUtilsProvider,KeyboardDatePicker,} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

function AgregarpersonaButtonModalPopup(){
	const [open, setOpen] = React.useState(false);
	const handleOpen = () => {
    	setOpen(true);
  	};
	const handleClose = () => {
	    setOpen(false);
	};
	return (
		<>
			<div className="agregar-btn-section">
			    <Button variant="contained" color="primary" size="large" id="agregar-btn" onClick={handleOpen} className="button">+ Agregar persona</Button>
			</div>
			<Modal
		        aria-labelledby="transition-modal-title"
		        aria-describedby="transition-modal-description"
		        className="modal"
		        open={open}
		        onClose={handleClose}
		        closeAfterTransition
		        BackdropComponent={Backdrop}
		        BackdropProps={{
		            timeout: 500,
		        }}
			>
				<Fade in={open}>
		   		 	<div className="paper">
		                <Grid item xs={12} className="popup-header-section">
		                  <Grid item xs={11} className="popup-head-text-section">
		                    <h2 className="popup-head">Agregar Nueva Persona</h2>
		                  </Grid>
		                  <Grid item xs={1} className="popup-close-btn-section">
		                    <div className="save-popup-close-section">
		                      <Button variant="contained" color="primary" size="large" id="close-save-popup-btn" onClick={handleClose} className="button">x</Button>
		                    </div>
		                  </Grid>
		                </Grid>
		        		<form className="container" noValidate autoComplete="off" id="popupinformaciónpersonal">
		          			<Grid item xs={12}>
		            			<div className="component-logged-in popup-left-form-section">
		              				<Grid item xs={6}>
		                				<div className="add-persona-info-picture-item-main-section">
		                                    <Button variant="contained" component="label">
			                                    <div className="add-persona-info-picture-item">
			                                      <CameraAltIcon fontSize="large" className="add-persona-info-picture-icon"/>
			                                    </div>
		                                    	<input type="file" style={{ display: "none" }}/>
		                                  	</Button>
		                				</div>
		                                <div className="input-field-section">
		                                  <TextField id="outlined-basic" defaultValue="0 0000 0000" label="ID Persona" variant="outlined" placeholder="0 0000 0000"/>
		                                </div>
		                                <div className="input-field-section">
		                                  <TextField id="outlined-basic" defaultValue="Juan" label="Nombre" variant="outlined" placeholder="Juan"/>
		                                </div>
		                				<div className="input-field-section">
		                                  	<MuiPickersUtilsProvider utils={DateFnsUtils}>
			                                    <KeyboardDatePicker
			                                      disableToolbar
			                                      required
			                                      variant="inline"
			                                      placeholder="00 / MM/ 0000"
			                                      format="MM/dd/yyyy"
			                                      margin="normal"
			                                      label="Fecha de Nacimiento"
			                                      inputVariant="outlined"
			                                      KeyboardButtonProps={{
			                                        'aria-label': 'change datedate',
			                                      }}
			                                    />
		                                  	</MuiPickersUtilsProvider>
		                				</div>
		              				</Grid>
		              				<Grid item xs={6}>
		                				<div className="component-logged-in icon-button-section popup-right-form-section">
		                  					<Grid item xs={12} className="half-input-section"> 
		                    					<Grid item xs={6} className="right-register-date-section">
		                      						<div className="input-field-section">
				                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
				                                          <KeyboardDatePicker
				                                            disableToolbar
				                                            required
				                                            variant="inline"
				                                            className="right-left-date-sec"
				                                            placeholder="00 / MM/ 0000"
				                                            format="MM/dd/yyyy"
				                                            margin="normal"
				                                            label="Registro"
				                                            inputVariant="outlined"
				                                            KeyboardButtonProps={{
				                                              'aria-label': 'change date',
				                                            }}
				                                          />
				                                        </MuiPickersUtilsProvider>
		                      						</div>
		                    					</Grid>
		                    					<Grid item xs={6} className="right-indica-labores-section">
		                      						<div className="input-field-section">
				                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
				                                          <KeyboardDatePicker
				                                            disableToolbar
				                                            required
				                                            variant="inline"
				                                            className="right-right-date-sec"
				                                            placeholder="00 / MM/ 0000"
				                                            format="MM/dd/yyyy"
				                                            margin="normal"
				                                            label="Inicio de labores"
				                                            inputVariant="outlined"
				                                            KeyboardButtonProps={{
				                                              'aria-label': 'change date',
				                                            }}
				                                        />
				                                        </MuiPickersUtilsProvider>
		                      						</div>
		                    					</Grid>
		                  					</Grid>
		                                  	<div className="input-field-section">
		                                    	<TextField id="outlined-basic" defaultValue="0 0000 0000" label="Identificación" variant="outlined" placeholder="0 0000 0000"/>
		                                  	</div>
		                                  	<div className="input-field-section">
		                                    	<TextField id="outlined-basic" defaultValue="Hernández Sancho" label="Apellidos" variant="outlined" placeholder="0 0000 0000"/>
		                                  	</div>
		                  					<div className="input-field-section">
		                    					<FormControl variant="outlined" required className="estado-civil-section">
		                      						<InputLabel id="estado-civil-label">Estado Civil</InputLabel>
			                                      	<Select labelId="estado-civil-label">
				                                        <MenuItem value="">
				                                          <em>Sin definir</em>
				                                        </MenuItem>
				                                        <MenuItem value="soltero">Soltero(a)</MenuItem>
				                                        <MenuItem value="casado">Casado(a)</MenuItem>
				                                        <MenuItem value="divorciado">Divorciado(a)</MenuItem>
				                                        <MenuItem value="viudo">Viudo(a)</MenuItem>
			                                      	</Select>
		                    					</FormControl>
		                  					</div>
		                				</div>
		              				</Grid>
		            			</div>
		          			</Grid>
		          			<Grid item xs={12}>
		            			<div className="component-logged-in popup-radio-button-section">
		                            <Grid item xs={4}>
		                                <FormControl component="fieldset" className="" required>
		                                  <FormLabel component="legend">Sexo</FormLabel>
		                                  <RadioGroup aria-label="sexo" name="sexo" row>
		                                    <FormControlLabel value="mujer" control={<Radio color="primary"/>} label="Mujer" />
		                                    <FormControlLabel value="hombre" control={<Radio color="primary"/>} label="Hombre" />
		                                  </RadioGroup>
		                                </FormControl>
		                          	</Grid>
		                          	<Grid item xs={4}>
		                                <FormControl component="fieldset" className="" required>
		                                  <FormLabel component="legend">Hijos</FormLabel>
		                                  <RadioGroup aria-label="hijos" name="hijos" row>
		                                    <FormControlLabel value="si" control={<Radio color="primary"/>} label="Sí" />
		                                    <FormControlLabel value="no" control={<Radio color="primary"/>} label="No" />
		                                  </RadioGroup>
		                                </FormControl>
		                          	</Grid>
		                          	<Grid item xs={4}>
		                                <FormControl component="fieldset" required>
		                                  <FormLabel component="legend">Licencia de conducir</FormLabel>
		                                  <RadioGroup aria-label="licencia de conducir" name="licencia-conducir" row>
		                                    <FormControlLabel value="si" control={<Radio color="primary"/>} label="Sí" />
		                                    <FormControlLabel value="no" control={<Radio color="primary"/>} label="No" />
		                                  </RadioGroup>
		                                </FormControl>
		                          	</Grid>
		            			</div>
		          			</Grid>
		          			<Grid item xs={12}>
		            			<div className="component-logged-in popup-observaciones-section">
		              				<Grid container className="form-input-row-container">
		                                <Grid item>
		                                  <TextField label="Observaciones" className="add-persona-info-observaciones" multiline rows="4" placeholder="Escriba aquí" margin="normal" variant="outlined"/>
		                                </Grid>
		              				</Grid>
		            			</div>
		          			</Grid>
		          			<Grid item xs={12} className="agregar-nueva-popup-button-section">
		              			<Button variant="contained" color="primary" size="large" id="agregar-nueva-popup-left-btn" className="button">Guardar</Button>
		              			<Button variant="contained" color="primary" size="large" id="agregar-nueva-popup-right-btn" className="classes.button">Cancelar</Button>
		          			</Grid>
		        		</form>
		    		</div>
				</Fade>
			</Modal>
		</>
	)
}

export default AgregarpersonaButtonModalPopup;
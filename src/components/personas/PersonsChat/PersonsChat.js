import React from 'react';
import '../Personas.css';
import {List, ListItem, ListItemText,} from '@material-ui/core';
import AttachmentIcon from '@material-ui/icons/Attachment';
import NearMeIcon from '@material-ui/icons/NearMe';

function PersonsChat() {
  const [selectedIndex, setSelectedIndex] = React.useState(1);
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  }
  return(
        <>
          <h3 className="chat-name-heading">Chat</h3>
          <List component="nav" aria-label="main mailbox folders" className="root">
              <ListItem button selected={selectedIndex === 0} onClick={event => handleListItemClick(event, 0)}>
                <div className="img_cont">
                  <img src="https://material-ui.com/static/images/avatar/1.jpg" className="rounded-circle user_img" alt=""/>
                  <span className="online_icon"></span>
                </div>
                <ListItemText primary="Juan Hernández" secondary={<React.Fragment>Ahora activo</React.Fragment>}/>
              </ListItem>
          </List>
          <div className="chat-dated-section">
            <span className="chat-dated">Hoy, 05.02.2018</span>
          </div>
          <ul className="chat-message">
            <li className="even-chat"><p className="emoji-icon-section"><span role="img" className="emoji-img" aria-label="">👋</span></p></li>
            <li className="odd-chat"><h3 className="user-name">Juan Hernández</h3><p className="user-message">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p></li>
            <li className="even-chat"><p className="user-message">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></li>
          </ul>
          <div className="chat-box-section">
            <p className="typing-text">Juan está escribiendo<span>...</span></p>
            <input type="text" name="chatinput" id="chatinput" value="" placeholder="Escribe un mensaje" />
            <AttachmentIcon className="attachment-icon"/> 
            <NearMeIcon className="nearme-icon"/>
          </div>
        </>
  )
}
export default PersonsChat;

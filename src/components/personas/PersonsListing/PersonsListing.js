import React from 'react';
import '../Personas.css';
import AgregarpersonaButtonModalPopup from '../AgregarpersonaButtonModalPopup/AgregarpersonaButtonModalPopup';
import {Grid, List, ListItem, ListItemText, ListItemAvatar, Avatar, InputBase, IconButton} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

function PersonsListing() {
		const [selectedIndex, setSelectedIndex] = React.useState(1);
		const [numberOfitemsShown, setNumberOfitemsShown] = React.useState(3);
		const handleListItemClick = (event, index) => {
	    	setSelectedIndex(index);
	  	};    
	  	const items = [
	    	{
		    	id: '1',
		    	image: 'https://material-ui.com/static/images/avatar/1.jpg',
		    	name: 'Juan Hernández',
		    	status: 'Active',
		  	},
		  	{
		    	id: '2',
			    image: 'https://material-ui.com/static/images/avatar/1.jpg',
			    name: 'Juan Hernández',
			    status: 'Active',
		  	},
			{
			    id: '3',
			    image: 'https://material-ui.com/static/images/avatar/1.jpg',
			    name: 'Juan Hernández',
			    status: 'Active',
			},
			{
			    id: '4',
			    image: 'https://material-ui.com/static/images/avatar/1.jpg',
			    name: 'Juan Hernández',
			    status: 'Active',
			},
			{
			    id: '5',
			    image: 'https://material-ui.com/static/images/avatar/1.jpg',
			    name: 'Juan Hernández',
			    status: 'Active',
			},
			{
			    id: '6',
			    image: 'https://material-ui.com/static/images/avatar/1.jpg',
			    name: 'Juan Hernández',
			    status: 'Active',
			},
			{
			    id: '7',
			    image: 'https://material-ui.com/static/images/avatar/1.jpg',
			    name: 'Juan Hernández',
			    status: 'Active',
			},
		  	{
		    	id: '8',
		    	image: 'https://material-ui.com/static/images/avatar/1.jpg',
		    	name: 'Juan Hernández',
		    	status: 'Active',
		  	}
	    ]
	    const showMore = (event) => {
		    if (numberOfitemsShown + 3 <= items.length) {
		      setNumberOfitemsShown(numberOfitemsShown + 3);
		    } else {
		      setNumberOfitemsShown(items.length);
		    }
	  	}
	return (
  		<div> 		
			<Grid item xs={12}>
				<h2 className="component-main-heading">Persona</h2>
				<p className="component-sub-heading">150  registrados</p>
	            <div className="search-section">
	              <InputBase className="input" placeholder="Buscar..." inputProps={{ 'aria-label': 'Buscar' }}/>
	              <IconButton type="submit" className="iconButton" aria-label="search"><SearchIcon /></IconButton>
	            </div>
	            <List component="nav" aria-label="main mailbox folders" className="user-list-root">
	              {items.slice(0, numberOfitemsShown).map(item =>( 
	                <ListItem button selected={selectedIndex === 0} onClick={event => handleListItemClick(event, 0)} key={item.id}>
	                <ListItemAvatar><Avatar alt="Remy Sharp" src={item.image} /></ListItemAvatar>
	                <ListItemText primary={item.name} secondary={<React.Fragment>{item.status}</React.Fragment>}/></ListItem>
	              ))}
	            </List>
	            <div className="show-more-btn-sec" style={numberOfitemsShown === items.length ? {display: 'none'} : {}}><button onClick={showMore}>Mostrar más <ExpandMoreIcon /></button></div>
	            <AgregarpersonaButtonModalPopup />
			</Grid>
  		</div>
	)
}
export default PersonsListing;
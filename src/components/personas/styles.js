import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
	inline: {
	    display: 'inline',
	},
	formControl: {
	    margin: theme.spacing(1),
	    minWidth: 120,
	    background: '#cdd0de',
	  	borderRadius: '20px',
	},
	selectEmpty: {
	    marginTop: theme.spacing(2),
	},
	textField: {
	    marginLeft: theme.spacing(1),
	    marginRight: theme.spacing(1),
	    width: '100%',
	},
	menu: {
	    width: 200,
	},
	media: {
	    height: 0,
	    paddingTop: '56.25%',
	    marginTop:'30'
	},
	table: {
	    minWidth: '100%',
	    border: '1px solid #ccc',
	},
	container: {
	  	height: '100vh',
	  	display: 'flex',
	  	flexWrap: 'wrap', 
	},
	input: {
		marginLeft: '8',
		flex: 1,
	},
	root: {
		width: '100%',
		height: 200,
		maxWidth: 360, 
		backgroundColor: '#fff',
	},
	userlistroot: {
	  	width: '100%',
	  	height: '200px',
	  	maxWidth: '360px', 
	  	backgroundColor: '#fff',
	},
	roottable: {
	  	width: '100%',
	  	height: '172px',
	  	maxWidth: '100%',
	  	backgroundColor: '#fff',
	},
	inline:{
		display: 'inline',
	},
	modal:{
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	paper:{
		backgroundColor: '#fff',
		border: '2px solid #000',
		boxShadow: '0px 3px 5px -1px rgba(0,0,0,0.2), 0px 5px 8px 0px rgba(0,0,0,0.14), 0px 1px 14px 0px rgba(0,0,0,0.12)',
	},
	iconButton:{
		padding: '5px !important',
		float: 'right',
	},
	componentmainheading: {
	  	marginBottom: '0px',
	  	marginTop: '0px',
	  	fontSize: '30px',
	},
	componentsubheading: {
	  	marginTop: '5px',
	},
	searchsection: {
	  	border: '2px solid #ccc',
	  	borderRadius: '5px',
	  	marginBottom: '40px',
	},
	iconsection: {
	  	width: '195px',
	  	border: '1px solid #ccc',
	  	borderRadius: '5px',
	  	height: '40px',
	},
	customsvgicon: {
	  	padding: '8px 20px',
	  	color: '#707070',
	  	borderRight: '1px solid #eee',
	  	cursor: 'pointer',
	},
	addpersonainfopictureitem: {
	    /*margin-top: 25px;*/
	    marginBottom: '25px',
	},
	addpersonainfopictureitemmainsection: {
	  	width: '100%',
	  	textAlign: 'center',
	},
	inputfieldsection: {
	  	width: '100%',
	  	border: '2px solid #ccc',
	  	borderRadius: '7px',
	  	marginBottom: '30px',
	},
	halfinputsection: {
	    display: 'flex',
	    /*margin-top: 49px !important;*/
	    marginBottom: '40px !important',
	},
	rightregisterdatesection: {
	    marginRight: '15px !important',
	},

	estadocivilsection: {
	    width: '100%',
	},
	chatnameheading: {
	  color: '#4674D0',
	},
	chatdatedsection: {
	    textAlign: 'center',
	    paddingTop: '20px',
	},
	chatdated: {
	    backgroundColor: '#DEEEFF',
	    padding: '5px 20px 5px 20px',
	    borderRadius: '20px',
	    fontSize: '12px',
	    color: '#4674D0',
	},
	chatmessage: {
	    listStyle: 'none',
	    paddingLeft: '0px',
	},
	oddchat: {
	    float: 'left',
	    padding: '0 10px 0px 10px',
	    backgroundColor: '#F0F3F8',
	    marginBottom: '20px',
	    width: '75%',
	},
	username: {
	    margin: '10px 0 0 0',
	    fontSize: '12px',
	    color: '#4674D0',
	},
	usermessage: {
	    marginTop: '5px',
	    fontSize: '14px',
	    color: '#121D39',
	},
	evenchat: {
	    backgroundColor: '#ECF2FF',
	    marginBottom: '20px',
	    borderRadius: '3px',
	    padding: '0 10px 0px 10px',
	    float: 'right',
	    width: '75%',
	},
	emojiimg: {
	    fontSize: '24px',
	    padding: '15px 30px 15px 30px',
	    color: '#FFCF4A',
	    float: 'right',
	},
	agregarbtnsection: {
	  textAlign: 'center',
	  position: 'absolute',
	  top: '100%',
	  left: '12%',
	},
}));
import React from 'react';
import '../Personas.css';
import PersonDetailsSaveModalPopup from '../PersonDetailsSaveModalPopup/PersonDetailsSaveModalPopup';
import PersonDetailsCloseModalPopup from '../PersonDetailsCloseModalPopup/PersonDetailsCloseModalPopup';
import {
  Grid, 
  TextField,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormLabel,
  RadioGroup,
  Radio,
  FormControlLabel,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';
import {MuiPickersUtilsProvider,KeyboardDatePicker,} from '@material-ui/pickers';
import EditIcon from '@material-ui/icons/Edit';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import DateFnsUtils from '@date-io/date-fns';
import Paper from '@material-ui/core/Paper';
import Autocomplete from '@material-ui/lab/Autocomplete';

function PersonsDetails() {
  const allItems = [
    { name: 'Información Personal1', id: 1994 },
    { name: 'Información Personal2', id: 1972 },
    { name: 'Información Personal3', id: 1974 },
    { name: 'Información Personal4', id: 2008 },
    { name: 'Información Personal5', id: 1957 },
  ];
  const [selectprofilevalue, setSelectProfile] = React.useState('informaciónpersonal');
  const [users, setUser] =  React.useState([{empleador: "", detallesdelaborales: "", desde: "", hasta: ""}]);

  
  
  
  const handleChange = (event) => {
    setSelectProfile(event.target.value);
  }
  const addClick = (prevState) => {
    const values = [...users];
    values.push({ empleador: "", detallesdelaborales: "", desde: "", hasta: "" });
    setUser(values);
  }
  const handleAddMoreFieldChange = (i, e) => { 
     const { name, value } = e.target;
     let values = [...users];
     values[i] = {...users[i], [name]: value};
     setUser(values);
  }
  

  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);
  const selectprofile = [
    {
      value: 'informaciónpersonal',
      label: 'Información Personal',
    },
    {
      value: 'contacto',
      label: 'Contacto',
    },
    {
      value: 'educación-experiencia',
      label: 'Educación / Experiencia',
    },
    {
      value: 'detalles-de-pago',
      label: 'Detalles de Pago',
    },
    {
      value: 'seguridad',
      label: 'Seguridad',
    },
  ];
		return(
      <>
			  <Grid item xs={12}>
					<div className="component-logged-in heading-section">
            <p className="left-breadcrumb-text"><span className="prev-text">Editando esta sección</span><span className="arrow-icon">></span> Juan Hernández</p>
          </div>
          <Grid item xs={12}>
            <div className="component-logged-in left-form-section two-field-section">
            	<Grid item xs={6} className="left-side-field-section">
                <div className="input-container dropdown-select-profile">
                  <TextField id="outlined-select-currency-native" select label="Sección" className="textField" onChange={handleChange} value={selectprofilevalue}
                    SelectProps={{native: true,MenuProps: {className: "menu",},}} margin="normal" variant="outlined">
                      {selectprofile.map(option => (
					              <option key={option.value} value={option.value}>{option.label}</option>
                    	))}
                  </TextField>
                </div>
              </Grid>
              <Grid item xs={6}>
              	<div className="component-logged-in left-form-section right-field-section">
              		<div className="icon_section">
              			<EditIcon />
                    <PersonDetailsSaveModalPopup/>
                    <PersonDetailsCloseModalPopup />
              		</div>
              	</div>
              </Grid>
            </div>
          </Grid>
			  </Grid>
        {/************************Information Personal Form**********************/}
        <form className="container" noValidate autoComplete="off" id="informaciónpersonal" style={selectprofilevalue === 'informaciónpersonal' ? {} : {display: 'none'}}>
          <Grid item xs={12}>
            <div className="component-logged-in left-form-section">
              <Grid item xs={6}>
                <div className="add-persona-info-picture-item-main-section">
                  <Button variant="contained" component="label">
                    <div className="add-persona-info-picture-item">
                      <CameraAltIcon fontSize="large" className="add-persona-info-picture-icon"/>
                    </div>
                    <input type="file" style={{ display: "none" }}/>
                  </Button>
                </div>
                <div className="input-field-section">
                  <TextField id="outlined-basic" defaultValue="0 0000 0000" label="ID Persona" variant="outlined" placeholder="0 0000 0000"/>
                </div>
                <div className="input-field-section">
                  <TextField id="outlined-basic" defaultValue="Juan" label="Nombre" variant="outlined" placeholder="Juan"/>
                </div>
                <div className="input-field-section">
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      disableToolbar
                      required
                      variant="inline"
                      placeholder="00 / MM/ 0000"
                      format="MM/dd/yyyy"
                      margin="normal"
                      label="Fecha de Nacimiento"
                      inputVariant="outlined"
                      KeyboardButtonProps={{
                        'aria-label': 'change datedate',
                      }}
                    />
                  </MuiPickersUtilsProvider>
                </div>
              </Grid>
              <Grid item xs={6}>
                <div className="component-logged-in icon-button-section right-form-section">
                  <Grid item xs={12} className="half-input-section"> 
                    <Grid item xs={6} className="right-register-date-section">
                      <div className="input-field-section">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardDatePicker
                            disableToolbar
                            required
                            variant="inline"
                            className="right-left-date-sec"
                            placeholder="00 / MM/ 0000"
                            format="MM/dd/yyyy"
                            margin="normal"
                            label="Registro"
                            inputVariant="outlined"
                            KeyboardButtonProps={{
                              'aria-label': 'change date',
                            }}
                          />
                        </MuiPickersUtilsProvider>
                      </div>
                    </Grid>
                    <Grid item xs={6} className="right-indica-labores-section">
                      <div className="input-field-section">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardDatePicker
                            disableToolbar
                            required
                            variant="inline"
                            className="right-right-date-sec"
                            placeholder="00 / MM/ 0000"
                            format="MM/dd/yyyy"
                            margin="normal"
                            label="Inicio de labores"
                            inputVariant="outlined"
                            KeyboardButtonProps={{
                              'aria-label': 'change date',
                            }}
                        />
                        </MuiPickersUtilsProvider>
                      </div>
                    </Grid>
                  </Grid>
                  <div className="input-field-section">
                    <TextField id="outlined-basic" defaultValue="0 0000 0000" label="Identificación" variant="outlined" placeholder="0 0000 0000"/>
                  </div>
                  <div className="input-field-section">
                    <TextField id="outlined-basic" defaultValue="Hernández Sancho" label="Apellidos" variant="outlined" placeholder="0 0000 0000"/>
                  </div>
                  <div className="input-field-section">
                    <FormControl variant="outlined" required className="estado-civil-section">
                      <InputLabel ref={inputLabel} id="estado-civil-label">Estado Civil</InputLabel>
                      <Select labelId="estado-civil-label" labelWidth={labelWidth}>
                        <MenuItem value="">
                          <em>Sin definir</em>
                        </MenuItem>
                        <MenuItem value="soltero">Soltero(a)</MenuItem>
                        <MenuItem value="casado">Casado(a)</MenuItem>
                        <MenuItem value="divorciado">Divorciado(a)</MenuItem>
                        <MenuItem value="viudo">Viudo(a)</MenuItem>
                      </Select>
                    </FormControl>
                  </div>
                </div>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="component-logged-in radio-button-section">
              <Grid item xs={4}>
                <FormControl component="fieldset" className="" required>
                  <FormLabel component="legend">Sexo</FormLabel>
                  <RadioGroup aria-label="sexo" name="sexo" row>
                    <FormControlLabel value="mujer" control={<Radio color="primary"/>} label="Mujer" />
                    <FormControlLabel value="hombre" control={<Radio color="primary"/>} label="Hombre" />
                  </RadioGroup>
                </FormControl>
              </Grid>
              <Grid item xs={4}>
                <FormControl component="fieldset" className="" required>
                  <FormLabel component="legend">Hijos</FormLabel>
                  <RadioGroup aria-label="hijos" name="hijos" row>
                    <FormControlLabel value="si" control={<Radio color="primary"/>} label="Sí" />
                    <FormControlLabel value="no" control={<Radio color="primary"/>} label="No" />
                  </RadioGroup>
                </FormControl>
              </Grid>
              <Grid item xs={4}>
                <FormControl component="fieldset" required>
                  <FormLabel component="legend">Licencia de conducir</FormLabel>
                  <RadioGroup aria-label="licencia de conducir" name="licencia-conducir" row>
                    <FormControlLabel value="si" control={<Radio color="primary"/>} label="Sí" />
                    <FormControlLabel value="no" control={<Radio color="primary"/>} label="No" />
                  </RadioGroup>
                </FormControl>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="component-logged-in observaciones-section">
              <Grid container className="form-input-row-container">
                <Grid item>
                  <TextField label="Observaciones" className="add-persona-info-observaciones" multiline rows="4" placeholder="Escriba aquí" margin="normal" variant="outlined"/>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </form>
        {/**********************Information Personal Form End*********************/}
        {/**********************Contacto Form*********************/}
        <form className="container" noValidate autoComplete="off" id="contacto" style={selectprofilevalue === 'contacto' ? {} : {display: 'none'}}>
          <Grid item xs={12}>
            <div className="component-logged-in left-form-section">
              <Grid item xs={6}>
                <div className="input-field-section">
                  <TextField id="outlined-basic" defaultValue="0000-0000" label="Teléfono  fijo" variant="outlined" placeholder="0000-0000"/>
                </div>
                <div className="input-field-section">
                  <TextField id="outlined-basic" label="Correo electrónico" variant="outlined"/>
                </div>
                <div className="input-field-section">
                  <FormControl variant="outlined" required className="estado-civil-section">
                    <InputLabel ref={inputLabel} id="estado-civil-label">Estado / Provincia </InputLabel>
                    <Select labelId="estado-civil-label" labelWidth={labelWidth}>
                      <MenuItem value="">
                        <em>Heredia</em>
                      </MenuItem>
                      <MenuItem value="heredia1">Heredia1</MenuItem>
                      <MenuItem value="heredia2">Heredia2</MenuItem>
                      <MenuItem value="heredia3">Heredia3</MenuItem>
                      <MenuItem value="heredia4">Heredia4</MenuItem>
                    </Select>
                  </FormControl>
                </div>
                <div className="input-field-section">
                  <FormControl variant="outlined" required className="estado-civil-section">
                    <InputLabel ref={inputLabel} id="estado-civil-label">Zona Horaria </InputLabel>
                    <Select labelId="estado-civil-label" labelWidth={labelWidth}>
                      <MenuItem value="">
                        <em>Zona Horaria1</em>
                      </MenuItem>
                      <MenuItem value="zona-horaria2">Zona Horaria2</MenuItem>
                      <MenuItem value="zona-horaria3">Zona Horaria3</MenuItem>
                      <MenuItem value="zona-horaria4">Zona Horaria4</MenuItem>
                      <MenuItem value="zona-horaria5">Zona Horaria5</MenuItem>
                    </Select>
                  </FormControl>
                </div>
              </Grid>
              <Grid item xs={6}>
                <div className="component-logged-in icon-button-section right-form-section">
                  <div className="input-field-section">
                    <TextField id="outlined-basic" defaultValue="0000-0000" label="Móvil" variant="outlined" placeholder="0000-0000"/>
                  </div>
                  <div className="input-field-section">
                    <FormControl variant="outlined" required className="estado-civil-section">
                      <InputLabel ref={inputLabel} id="estado-civil-label">País</InputLabel>
                      <Select labelId="estado-civil-label" labelWidth={labelWidth}>
                        <MenuItem value="">
                          <em>Costa Rica</em>
                        </MenuItem>
                        <MenuItem value="costa-rica1">Costa Rica1</MenuItem>
                        <MenuItem value="costa-rica2">Costa Rica2</MenuItem>
                        <MenuItem value="costa-rica3">Costa Rica3</MenuItem>
                        <MenuItem value="costa-rica4">Costa Rica4</MenuItem>
                      </Select>
                    </FormControl>
                  </div>
                  <div className="input-field-section">
                    <FormControl variant="outlined" required className="estado-civil-section">
                      <InputLabel ref={inputLabel} id="estado-civil-label">Ciudad</InputLabel>
                      <Select labelId="estado-civil-label" labelWidth={labelWidth}>
                        <MenuItem value="">
                          <em>San Rafael</em>
                        </MenuItem>
                        <MenuItem value="san-rafael1">San Rafael1</MenuItem>
                        <MenuItem value="san-rafael2">San Rafael2</MenuItem>
                        <MenuItem value="san-rafael3">San Rafael3</MenuItem>
                        <MenuItem value="san-rafael4">San Rafael4</MenuItem>
                      </Select>
                    </FormControl>
                  </div>
                  <div className="input-field-section">
                    <TextField id="outlined-basic" label="Código Postal" variant="outlined"/>
                  </div>
                </div>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="component-logged-in radio-button-section">
              <div className="input-field-section">
                <TextField id="outlined-basic" label="PIN Google Maps" variant="outlined"/>
              </div>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="component-logged-in radio-button-section">
              <div className="input-field-section">
                <TextField id="outlined-basic" label="Dirección" variant="outlined"/>
              </div>
            </div>
          </Grid>
        </form>
        {/**********************Contacto Form End*********************/}
        {/*********************Educación Experiencia Form************************/}
        <form className="container" noValidate autoComplete="off" id="educación-experiencia" style={selectprofilevalue === 'educación-experiencia' ? {} : {display: 'none'}}>
          <Grid item xs={12}>
            <div className="component-logged-in left-form-section">
              <Grid item xs={6}>
                <div className="input-field-section">
                  <TextField id="outlined-basic" defaultValue="Escriba aquí" label="Profesión" variant="outlined" placeholder="Escriba aquí"/>
                </div>
                <Autocomplete
                    multiple
                    id="tags-standard"
                    className="multiple-select-box-section"
                    options={allItems}
                    getOptionLabel={option => option.name}
                    renderInput={params => (
                      <TextField
                        {...params}
                        variant="standard"
                        label="Multiple values"
                        placeholder="Favorites"
                        fullWidth
                      />
                    )}
                />
              </Grid>
              <Grid item xs={6}>
                <div className="component-logged-in icon-button-section right-form-section">
                  <div className="input-field-section">
                    <TextField id="outlined-basic" defaultValue="Escriba aquí" label="Idioma" variant="outlined" placeholder="Escriba aquí"/>
                  </div>
                  <div className="component-logged-in educación-experiencia-observaciones-section">
                    <Grid container className="form-input-row-container otros-estudios-text-section">
                      <Grid item>
                        <TextField label="Otros Estudios" className="add-persona-info-observaciones" multiline rows="4" placeholder="Escriba aquí" margin="normal" variant="outlined"/>
                      </Grid>
                    </Grid>
                  </div>
                </div>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="component-logged-in observaciones-section">
               
                <div className="pera-text">
                <Grid item xs={12} className="big-pera">
                  <Grid item xs={3}><p>Empleador</p></Grid>
                  <Grid item xs={5}><p>Detalles de laborales</p></Grid>
                  <Grid item xs={1}><p>Desde</p></Grid>
                  <Grid item xs={3}><p>Hasta</p></Grid>
                </Grid>

                </div>
              {users.map((el, i) => (
                 <div key={i}>
                    <input className="main-css" placeholder="" name="empleador" value={el.empleador ||''} onChange={handleAddMoreFieldChange.bind(this, i)} />
                    <input className="main-css2" placeholder="" name="detallesdelaborales" value={el.detallesdelaborales ||''} onChange={handleAddMoreFieldChange.bind(this, i)} />
                    <input className="main-css3" placeholder="" name="desde" value={el.desde ||''} onChange={handleAddMoreFieldChange.bind(this, i)} />
                    <input className="main-css3" placeholder="" name="hasta" value={el.hasta ||''} onChange={handleAddMoreFieldChange.bind(this, i)} />
                    {/*<input type='button' value='remove' onClick={removeClick.bind(this, i)}/>*/}
                 </div>          
              ))}
              <div className="add-more-field-btn-sec">
                <span onClick={addClick.bind(this)} className="add-more-field-btn">+ Añadir más</span>
              </div>
              {/*<input type='button' value='add more' onClick={addClick.bind(this)}/>*/}
            </div>
          </Grid>
        </form>

        {/*********************Educación Experiencia Form End************************/}
        {/*********************Detalles De Pago Form************************/}
        <form className="container" noValidate autoComplete="off" id="detalles-de-pago" style={selectprofilevalue === 'detalles-de-pago' ? {} : {display: 'none'}}>
          <Grid item xs={12}>
            <div className="component-logged-in left-form-section">
              <Grid item xs={6}>
                <h2 className="left-first-head-text">Tarifa de la persona por hora</h2>
                <Grid item xs={12} className="left-first-field-section">
                  <div className="input-field-section detalles-de-pago-left-field-section">
                    <TextField id="outlined-basic" defaultValue="00000" label="Monto" variant="outlined" placeholder="00000"/>
                  </div>
                  <div className="input-field-section detalles-de-pago-right-field-section">
                    <FormControl variant="outlined" required className="estado-civil-section">
                      <InputLabel ref={inputLabel} id="estado-civil-label">Moneda</InputLabel>
                      <Select labelId="estado-civil-label" labelWidth={labelWidth}>
                        <MenuItem value="">
                          <em>colones</em>
                        </MenuItem>
                        <MenuItem value="colones1">colones1</MenuItem>
                        <MenuItem value="colones2">colones2</MenuItem>
                        <MenuItem value="colones3">colones3</MenuItem>
                        <MenuItem value="colones4">colones4</MenuItem>
                      </Select>
                    </FormControl>
                  </div>
                </Grid>
                <h2 className="left-first-head-text">Método de pago</h2>
                <div className="input-field-section">
                    <FormControl variant="outlined" required className="estado-civil-section">
                      <InputLabel ref={inputLabel} id="estado-civil-label">Entidad Bancaria</InputLabel>
                      <Select labelId="estado-civil-label" labelWidth={labelWidth}>
                        <MenuItem value="">
                          <em>Seleccionar....</em>
                        </MenuItem>
                        <MenuItem value="seleccionar1">Seleccionar1</MenuItem>
                        <MenuItem value="seleccionar2">Seleccionar2</MenuItem>
                        <MenuItem value="seleccionar3">Seleccionar3</MenuItem>
                        <MenuItem value="seleccionar4">Seleccionar4</MenuItem>
                      </Select>
                    </FormControl>
                </div>
                <div className="input-field-section">
                  <TextField id="outlined-basic" label="IBAN" variant="outlined" placeholder="Escriba aquí"/>
                </div>
              </Grid>
              <Grid item xs={6}>
                <div className="component-logged-in icon-button-section right-form-section">
                  <Grid item xs={12} className="half-input-section"> 
                    <Grid item xs={6} className="right-register-date-section">
                    </Grid>
                    <Grid item xs={6} className="right-indica-labores-section">
                    </Grid>
                  </Grid>
                </div>
              </Grid>
            </div>
          </Grid>
        </form>
        {/**************************Detalles De Pago Form End*************************/}
        {/**************************Seguridad Form*************************/}
        <form className="container" noValidate autoComplete="off" id="seguridad" style={selectprofilevalue === 'seguridad' ? {} : {display: 'none'}}>
          <Grid item xs={12}>
            <div className="component-logged-in left-form-section">
              <Grid item xs={6}>
                <div className="input-field-section">
                  <FormControl variant="outlined" required className="estado-civil-section">
                    <InputLabel ref={inputLabel} id="estado-civil-label">Seleccione categoría</InputLabel>
                    <Select labelId="estado-civil-label" labelWidth={labelWidth}>
                      <MenuItem value="">
                        <em>Empleado</em>
                      </MenuItem>
                      <MenuItem value="empleado1">Empleado1</MenuItem>
                      <MenuItem value="empleado2">Empleado2</MenuItem>
                      <MenuItem value="empleado3">Empleado3</MenuItem>
                      <MenuItem value="empleado4">Empleado4</MenuItem>
                    </Select>
                  </FormControl>
                </div> 
              </Grid>
              <Grid item xs={6}>
                <div className="component-logged-in icon-button-section right-form-section">
                  <Grid item xs={12} className="half-input-section"> 
                    <Grid item xs={6} className="right-register-date-section">
                    </Grid>
                    <Grid item xs={6} className="right-indica-labores-section">
                    </Grid>
                  </Grid>
                </div>
              </Grid>
            </div>
          </Grid>
          <div className="component-logged-in acceso-para-head-section">
            <h2 className="left-first-head-text">Acceso para la aplicación móvil</h2> 
          </div>
          <Grid item xs={12}>
            <div className="component-logged-in radio-button-section">
              <Grid item xs={7} className="usuario-text-field-section">
                <div className="input-field-section">
                  <TextField id="outlined-basic" label="Usuario" variant="outlined" placeholder=""/>
                </div>
              </Grid>
              <Grid item xs={5}>
                <Button variant="contained" color="primary" size="large" id="enviar-btn" className="button">Enviar invitación</Button>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="component-logged-in radio-button-section">
              <Grid item xs={7} className="usuario-text-field-section">
                <div className="input-field-section">
                  <TextField id="outlined-basic" label="Contraseña" variant="outlined" placeholder=""/>
                </div>
              </Grid>
              <Grid item xs={5}>
                <Button variant="contained" color="primary" size="large" id="enviar-btn" className="button">Generar nueva</Button>
              </Grid>
            </div>
          </Grid>
          <div className="component-logged-in acceso-para-head-section">
            <h2 className="left-first-head-text">Unidades de trabajo Activas y asignadas</h2> 
          </div>
          <Grid item xs={12}>
            <div className="component-logged-in radio-button-section">
              <Grid item xs={9} className="usuario-text-field-section">
                <Paper className="roottable">
                  <Table className="table" aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell>Unidad de Trabajo</TableCell>
                        <TableCell align="right">PIN para Kiosco</TableCell>
                        <TableCell align="right">Enviar PIN</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow key="Equipo 1">
                          <TableCell component="th" scope="row">Equipo 1</TableCell>
                          <TableCell align="right">55424</TableCell>
                          <TableCell align="right"><ChatBubbleOutlineIcon/> <MailOutlineIcon/></TableCell>
                        </TableRow>
                        <TableRow key="Equipo 2">
                          <TableCell component="th" scope="row">Equipo 2</TableCell>
                          <TableCell align="right">No posee</TableCell>
                          <TableCell align="right"></TableCell>
                        </TableRow>
                    </TableBody>
                  </Table>
                </Paper>
              </Grid>
            </div>
          </Grid>
        </form>
        {/**************************Seguridad Form End*************************/}
      </>
		)
}

export default PersonsDetails;
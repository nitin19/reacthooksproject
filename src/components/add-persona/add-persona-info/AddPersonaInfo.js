import React from 'react'
import { Grid, 
  TextField, 
  FormControl, 
  InputLabel, 
  Select, 
  MenuItem,
  RadioGroup, 
  Radio,
  FormLabel,
  FormControlLabel } from '@material-ui/core'
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import './addpersonainfo.css';

function AddPersonaInfo() {
  
  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);


  return (
    <div>
        <Grid container className="add-persona-info-container">
          <Grid item className="add-persona-info-picture-container">
            <div className="add-persona-info-picture-item">
              <CameraAltIcon fontSize="large" className="add-persona-info-picture-icon"/>
            </div>
          </Grid>
          <Grid item className="add-persona-info-form-container">
            <p>Completa la siguiente información </p>
            <Grid container className="add-persona-info-form-grid">
              <Grid item>
                <Grid container className="form-input-row-container">
                  <Grid item>
                  <TextField
                    required
                    label="ID Persona"
                    defaultValue="0 0000 0000"
                    className="form-textfield-input"
                    margin="normal"
                    variant="outlined"
                  />
                  </Grid>
                  <Grid item>
                  <TextField
                    required
                    label="Identificación"
                    defaultValue="0 0000 0000"
                    className="form-textfield-input"
                    margin="normal"
                    variant="outlined"
                  />
                  </Grid>
                  <Grid item>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        required
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        label="Registro"
                        inputVariant="outlined"
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        required
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        label="Inicio de Labores"
                        inputVariant="outlined"
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid container className="form-input-row-container">
                  <Grid item>
                  <TextField
                    required
                    label="Nombre"
                    defaultValue="Laura"
                    className="form-textfield-input"
                    margin="normal"
                    variant="outlined"
                  />
                  </Grid>
                  <Grid item>
                  <TextField
                    required
                    label="Apellidos"
                    defaultValue="Hernández Sancho"
                    className="form-textfield-input"
                    margin="normal"
                    variant="outlined"
                  />
                  </Grid>
                  <Grid item>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        required
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        label="Fecha de Nacimiento"
                        inputVariant="outlined"
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid container className="form-input-row-container">
                  <Grid item>
                    <FormControl variant="outlined" required>
                      <InputLabel ref={inputLabel} id="estado-civil-label">
                        Estado Civil
                      </InputLabel>
                      <Select
                        labelId="estado-civil-label"
                        labelWidth={labelWidth}
                      >
                        <MenuItem value="">
                          <em>Sin definir</em>
                        </MenuItem>
                        <MenuItem value="soltero">Soltero(a)</MenuItem>
                        <MenuItem value="casado">Casado(a)</MenuItem>
                        <MenuItem value="divorciado">Divorciado(a)</MenuItem>
                        <MenuItem value="viudo">Viudo(a)</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item>
                    <FormControl component="fieldset" className="" required>
                      <FormLabel component="legend">Sexo</FormLabel>
                      <RadioGroup aria-label="sexo" name="sexo" row>
                        <FormControlLabel value="mujer" control={<Radio color="primary"/>} label="Mujer" />
                        <FormControlLabel value="hombre" control={<Radio color="primary"/>} label="Hombre" />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item>
                    <FormControl component="fieldset" className="" required>
                      <FormLabel component="legend">Hijos</FormLabel>
                      <RadioGroup aria-label="hijos" name="hijos" row>
                        <FormControlLabel value="si" control={<Radio color="primary"/>} label="Sí" />
                        <FormControlLabel value="no" control={<Radio color="primary"/>} label="No" />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item>
                    <FormControl component="fieldset" required>
                      <FormLabel component="legend">Licencia de conducir</FormLabel>
                      <RadioGroup aria-label="licencia de conducir" name="licencia-conducir" row>
                        <FormControlLabel value="si" control={<Radio color="primary"/>} label="Sí" />
                        <FormControlLabel value="no" control={<Radio color="primary"/>} label="No" />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid container className="form-input-row-container">
                  <Grid item>
                    <TextField
                      label="Observaciones"
                      className="add-persona-info-observaciones"
                      multiline
                      rows="4"
                      placeholder="Escriba aquí"
                      margin="normal"
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
    </div>
  )
}

export default AddPersonaInfo

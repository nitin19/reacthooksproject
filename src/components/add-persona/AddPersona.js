import React from 'react';
import { Grid } from '@material-ui/core';
import AddPersonaInfo from './add-persona-info/AddPersonaInfo'

import './addpersona.css';


function AddPersona() {
  return (
    <div className="component-logged-in">
       <Grid container className="addpersona-container">
         <Grid item className="addpersona-container-izq">
          <h2>Agregar Persona</h2>
          <Grid container className="multistep-container">
              <Grid item className="info-personal-multistep-header">Información Personal</Grid>
              <Grid item>Contacto</Grid>
              <Grid item>Experiencia</Grid>
              <Grid item>Detalles de Pago</Grid>
              <Grid item className="seguridad-multistep-header">Seguridad</Grid>
          </Grid>
         </Grid>
       </Grid>
       <AddPersonaInfo/>
    </div>
  )
}

export default AddPersona;

import React from 'react';
import { Grid, Button } from  '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import FlashOnIcon from '@material-ui/icons/FlashOn';
import './topbar.css';

function TopBar() {
  return (
    <div>
       <AppBar position="fixed">
        <Toolbar>
            <Grid container className="grid-container-topbar">
              <Grid item className="grid-item-topbar-left"></Grid>
              <Grid item className="grid-item-topbar-right">
                <div className="icon-topbar-flash-container">
                  <FlashOnIcon fontSize="inherit" className="icon-topbar-flash"/> 
                </div>
                <div className="users-right-topbar-text">
                  Plan Básico <span>(20 de 30 usuarios)</span>
                </div>
                <div className="btn-topbar-plan-container">
                  <Button color="primary" className="btn-topbar-plan">Aumentar plan</Button>
                </div>
              </Grid>
            </Grid>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default TopBar;

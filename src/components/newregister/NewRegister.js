import React from 'react';
import './Newregister.css';
import clsx from 'clsx';
import {Grid, TextField, Button, InputLabel, InputAdornment, FormControl, Input, IconButton, FormHelperText} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { makeStyles } from '@material-ui/core/styles';
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import newlogo from '../../images/newlogo.png';
import leftimg from '../../images/register-left-img.jpg';
import useForm from "./useForm";
import validate from './registerFormValidationRules';

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1),
  },
}));
function NewRegister() {
  const classes = useStyles();
  const [showPassword, setShowpassValue] = React.useState(false);
  const [showConfirmPassword, setConfirmShowpassValue] = React.useState(false);
  const {
    values,
    errors,
    handleSubmit,
    handleChange,
  } = useForm(login, validate);

  const handleClickShowPassword = () => {
    setShowpassValue({ ...values, showPassword: !showPassword });
  };
  const handleConfirmPassClickShowPassword = () => {
    setConfirmShowpassValue({ ...values, showConfirmPassword: !showConfirmPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };
  const handleConfirmPassMouseDownPassword = event => {
    event.preventDefault();
  };

  function login() {
    console.log('No errors, submit callback called!');
  }
  return (
    <div>
      <Grid container className="container">
        <Grid item xs={2}></Grid>
        <Grid item xs={8}>
          <div className="component-logged-in rg-main-content-section">
              <div className="rg-logo-section">
                <Grid item xs={6}>
                  <img className="rg-logo-img" src={newlogo} alt="logo" />
                </Grid>
                <Grid item xs={6}>
                </Grid>
              </div>
              <div className="rg-form-sub-section">
                <Grid item xs={5} className="rg-form-section">
                    <h2 className="rg-main-head">Crear tu cuenta en Orquesti</h2>
                    <form onSubmit={handleSubmit} className="root" noValidate autoComplete="off">
                      <TextField 
                        id="input-with-icon-textfield" 
                        className={classes.margin} 
                        label="Nombre y apellido" 
                        name="name"
                        value={values.name||''}
                        onChange={handleChange}
                        required
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end"> 
                              <PersonOutlineOutlinedIcon color="disabled"/> 
                            </InputAdornment>
                          ),
                        }} 
                      />
                      {errors.name && (
                        <p className="help is-danger">{errors.name}</p>
                      )}
                      <FormControl className={clsx(classes.margin, classes.textField)}>
                        <InputLabel htmlFor="my-input">Correo</InputLabel>
                        <Input 
                          id="my-input" 
                          aria-describedby="my-helper-text"
                          type="email"
                          name="email"
                          value={values.email||''}
                          onChange={handleChange}
                          required
                          endAdornment={ 
                              <InputAdornment position="end"> 
                                <MailOutlineIcon color="disabled"/> 
                              </InputAdornment>
                          }
                        />
                        {errors.email && (
                          <p className="help is-danger">{errors.email}</p>
                        )}
                      </FormControl>
                      <FormControl className={clsx(classes.margin, classes.textField)}>
                        <InputLabel htmlFor="standard-adornment-password">Contrasena</InputLabel>
                        <Input
                          id="standard-adornment-password"
                          type={values.showPassword ? 'text' : 'password'}
                          name="password"
                          value={values.password||''}
                          onChange={handleChange}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                              color="disabled" >
                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                        {errors.password && (
                          <p className="help is-danger">{errors.password}</p>
                        )}
                      </FormControl>
                      <FormControl className={clsx(classes.margin, classes.textField)}>
                        <InputLabel htmlFor="standard-adornment-password">Confirmacion</InputLabel>
                        <Input
                          id="standard-adornment-password3"
                          type={values.showConfirmPassword ? 'text' : 'password'}
                          name="confirmpassword"
                          value={values.confirmpassword||''}
                          onChange={handleChange}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleConfirmPassClickShowPassword}
                                onMouseDown={handleConfirmPassMouseDownPassword}
                              color="disabled" >
                                {values.showConfirmPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                        {errors.confirmpassword && (
                          <p className="help is-danger">{errors.confirmpassword}</p>
                        )}
                        <FormHelperText id="my-helper-text">Usa 8 o mas caracteres con una combinacion de letras, numeros y simbolos.</FormHelperText>
                      </FormControl>
                      <div className="rg-button-section">
                        <Grid item xs={6} className="rg-left-btn-section">
                          <Button className="rg-cuenta-rgscripcion-aumentar-btn">Acceder a tu cuenta</Button>
                        </Grid>
                        <Grid item xs={6} className="rg-right-btn-section">
                          <Button type="submit" className="rg-cuenta-rgscripcion-aumentar-btn">Siguiente</Button>
                        </Grid>
                      </div>
                    </form>
                </Grid>
                <Grid item xs={7} className="rg-image-content-section">
                  <div className="rg-image-section">
                    <img src={leftimg} alt="" />
                  </div>
                  <p className="rg-right-text">Gestiona el tiempo de tus equipos, y administra el recurso de forma eficiente</p>
                </Grid>
              </div>
          </div>
          <div className="component-logged-in rg-footer-section">
            <ul className="rg-footer-menu-section">
              <li><a href="/" className="rg-footer-menu-link">Ayuda</a></li>
              <li><a href="/" className="rg-footer-menu-link">Privacidad</a></li>
              <li><a href="/" className="rg-footer-menu-link">Condiciones</a></li>
            </ul>
          </div>
        </Grid>
        <Grid item xs={2}></Grid>
      </Grid>
    </div>
  )
}

export default NewRegister;
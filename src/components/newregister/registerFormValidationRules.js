export default function validate(values) {
  let errors = {};
  if (!values.name) {
    errors.name = 'Nombre y apellido is required';
  } else if (!/\S+[A-Za-z]\S+/.test(values.name)) {
    errors.name = 'Nombre y apellido is invalid';
  }
  if (!values.email) {
    errors.email = 'Este is required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Este correo ya está en uso. Elige otro.';
  }
  if (!values.password) {
    errors.password = 'Las contraseñas is required';
  } else if (values.password.length < 8) {
    errors.password = 'Ingrese al menos 8 caracteres para la contraseña.';
  }
  if (!values.confirmpassword) {
    errors.confirmpassword = 'Las contraseñas is required';
  } else if (values.confirmpassword.length < 8) {
    errors.confirmpassword = 'Ingrese al menos 8 caracteres para la contraseña.';
  } else if (values.password !== values.confirmpassword) {
    errors.confirmpassword = '¡La contraseña no coincide!';
  }
  return errors;
};
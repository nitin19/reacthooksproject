export default function validate(values) {
  let errors = {};
  if (!values.email) {
    errors.email = 'Este is required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Este correo ya está en uso. Elige otro.';
  }
  if (!values.password) {
    errors.password = 'Las contraseñas is required';
  } else if (values.password.length < 8) {
    errors.password = 'Ingrese al menos 8 caracteres para la contraseña.';
  }
  return errors;
};

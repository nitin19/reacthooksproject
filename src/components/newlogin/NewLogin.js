import React from 'react';
import './Newlogin.css';
import useForm from "./useForm";
import validate from './LoginFormValidationRules';
import clsx from 'clsx';
import {Grid, Checkbox, Button, FormControl, InputLabel, Input, InputAdornment, IconButton} from '@material-ui/core';
import {Link} from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import newlogo from '../../images/newlogo.png';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1),
  },
}));
function NewLogin() {
  const classes = useStyles();
  //const [checked, setChecked] = React.useState(true);
  const [showPassword, setShowpassValue] = React.useState(false);
  const {
    values,
    errors,
    handleSubmit,
    handleChange,
  } = useForm(login, validate);

  function login() {
    console.log('No errors, submit callback called!');
  }

  /*const handleChangeCheckbox = event => {
    setChecked(event.target.checked);
  };*/
  const handleClickShowPassword = () => {
    setShowpassValue({showPassword: !showPassword });
  };
  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  
  return (
    <div>
      <Grid container className="container">
        <Grid item xs={4}></Grid>
        <Grid item xs={4}>
          <div className="component-logged-in lg-main-content-section">
            <div className="lg-logo-section">
              <Grid item xs={12}>
                <img className="lg-logo-img" src={newlogo} alt="logo" />
              </Grid>
            </div>
            <Grid item xs={12}>
                <h2 className="lg-main-head">Acceder a Orquesti</h2>
                <form onSubmit={handleSubmit} className="lg-login-form" noValidate autoComplete="off">
                  <FormControl className={clsx(classes.margin, classes.textField)}>
                    <InputLabel htmlFor="my-input">Correo</InputLabel>
                    <Input 
                      id="my-input" 
                      type="email" 
                      name="email"
                      onChange={handleChange}
                      value={values.email || ''} 
                      required
                      aria-describedby="my-helper-text"
                      className={`input ${errors.email && 'is-danger'}`}
                      endAdornment={ 
                          <InputAdornment position="end"> 
                            <MailOutlineIcon color="disabled"/> 
                          </InputAdornment>
                      }
                    />
                    {errors.email && (
                      <p className="help is-danger">{errors.email}</p>
                    )}
                  </FormControl>
                  <FormControl className={clsx(classes.margin, classes.textField)}>
                    <InputLabel htmlFor="standard-adornment-password">Contrasena</InputLabel>
                    <Input
                      id="standard-adornment-password"
                      className={`input ${errors.password && 'is-danger'}`}
                      name="password"
                      type={showPassword ? 'text' : 'password'}
                      value={values.password}
                      onChange={handleChange}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                          color="disabled" >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    {errors.password && (
                      <p className="help is-danger">{errors.password}</p>
                    )}
                  </FormControl>
                  <div className="lg-link-section">
                    <Grid item xs={7} className="lg-left-link-section">
                      <Link to="/newregister" className="lg-link-register"><p>¿Olvidaste tu contrasena?</p></Link>
                    </Grid>
                    <Grid item xs={5} className="lg-right-link-section">
                      <Checkbox
                        defaultChecked
                        value="secondary"
                        color="primary"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />Recordarme
                    </Grid>
                  </div>
                  <div className="lg-button-section">
                    <Grid item xs={6} className="lg-left-btn-section">
                      <Button className="lg-cuenta-rgscripcion-aumentar-btn">Crear cuenta</Button>
                    </Grid>
                    <Grid item xs={6} className="lg-right-btn-section">
                      <Button type="submit" className="lg-cuenta-rgscripcion-aumentar-btn">Iniciar sesion</Button>
                    </Grid>
                  </div>
                </form>
            </Grid>
          </div>
          <div className="component-logged-in lg-footer-section">
            <ul className="lg-footer-menu-section">
              <li><a href="/" className="lg-footer-menu-link">Ayuda</a></li>
              <li><a href="/" className="lg-footer-menu-link">Privacidad</a></li>
              <li><a href="/" className="lg-footer-menu-link">Condiciones</a></li>
            </ul>
          </div>
        </Grid>
        <Grid item xs={4}></Grid>
      </Grid>
    </div>
  )
}

export default NewLogin;
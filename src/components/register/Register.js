import React from 'react';
import './register.css';
import {Grid, TextField, Button} from '@material-ui/core';
import {Link} from 'react-router-dom';

function Register() {
  return (
    <div>
      <Grid container className="container">
        <Grid item xs={6}>
          <Grid container>
            <Grid item xs={4}></Grid>  
            <Grid item xs={4}>
              <div className="login-container">
                <div className="titulo">
                  <h1>Comencemos</h1>
                </div>
                <div className="sin-cuenta">
                <Link to="/login" className="link-login-con-cuenta"><p>¿Ya tienes una cuenta?</p></Link>
                </div> 
                <div className="form-container">
                  <div className="input-container">
                    <TextField
                      id="standard-required"
                      label="Nombre completo"
                      defaultValue=""
                      className="TextField"
                      margin="normal"
                      color="secondary"
                    />
                  </div>
                  <div className="input-container">
                    <TextField
                      id="standard-required"
                      label="Correo"
                      defaultValue=""
                      className="TextField"
                      margin="normal"
                      color="secondary"
                    />
                  </div>
                  <div className="input-container">
                    <TextField
                      id="standard-required"
                      label="Contraseña"
                      defaultValue=""
                      className="TextField"
                      type="password"
                      margin="normal"
                      color="secondary"
                    />
                  </div>
                  <div className="register-container-botones">
                    <Button variant="contained" color="secondary">Crear Cuenta</Button>
                    <Link to="/login" className="register-login-btn"><Button color="inherit">Iniciar Sesión</Button></Link>
                  </div>
                </div>
              </div>
            </Grid>  
            <Grid item xs={4}></Grid>  
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <div className="panel-derecha">
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default Register;
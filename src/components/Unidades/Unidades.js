import React from 'react';
import './Unidades.css';
import {Grid, Button, IconButton, InputBase} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import Calendarioempty from '../../images/Calendarioempty.png';


function Unidades() {
  return (
    <div>
      <Grid container className="container">
        <Grid item xs={3} className="ud-sidebar-section">
          <div className="component-logged-in ud-side-content-section">
            <h2 className="ud-side-top-head">Unidades de trabajo</h2>
            <div className="search-section">
                <InputBase className="input" placeholder="Buscar..." inputProps={{ 'aria-label': 'Buscar' }}/>
                <IconButton type="submit" className="iconButton" aria-label="search"><SearchIcon /></IconButton>
            </div>
            <p className="ud-error-text">¡Oh! Parece que no hay nadie por acá</p>
            <div className="agregar-btn-section">
              <Button variant="contained" color="primary" size="large" id="agregar-btn" className="button">+ Agregar persona</Button>
            </div>
          </div>
        </Grid>
        <Grid item xs={9}>
          <div className="component-logged-in ud-right-content-section">
            <h2 className="ud-right-content-top-head">Bienvenido a su calendario de Orquesti</h2>
            <p className="ud-right-sub-text">Administre a sus equipos por unidades de trabajo y verifique los tiempos laborales</p>
            <div className="ud-right-bottom-section">
              <img src={Calendarioempty} alt="" />
              <p className="ud-right-bottom-text">Por el momento no hay empleados presentes en su base de datos, ingrese nuevos miembros de su equipo para comenzar a administrar su horarios.</p>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default Unidades;